# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 13:31:52 2022

@author: Lothar Maisenbacher/Berkeley

Class representing a Gaussian laser beam.
"""

import math
import numpy as np
import scipy.special

class GaussianBeam:
    """
    A Gaussian laser beam, characterized by a complex beam parameter `q` and a wavelength
    `wavelength`.
    """
    # Default complex beam parameter: q = z + izr
    DEFAULT_Q = np.nan + 1j*np.nan
    # Default wavelength (m)
    DEFAULT_WAVELENGTH = np.nan

    def __init__(self, q=DEFAULT_Q, wavelength=DEFAULT_WAVELENGTH, w0=None):
        """Initialize Gaussian beam with complex beam parameter `q` and wavelength `wavelength`."""
        self.q = q
        self.wavelength = wavelength
        if w0 is not None:
            self.w0 = w0

    def __copy__(self):
        """
        Return a copy of this class with identical complex beam parameter `self.q` and wavelength
        `self.wavelength`.
        """
        return GaussianBeam(self.q, self.wavelength)

    def get_params_(self, q, wavelength):
        """Get values of parameters `q` and `wavelength`."""
        q = self.q if q is None else q
        wavelength = self.wavelength if wavelength is None else wavelength
        return q, wavelength        
    
    def get_w0(self, q=None, wavelength=None):
        """
        Calculate beam waist `w0` (1/e^2 intensity radius) from complex beam parameter `q`
        and wavelength `wavelength`.
        """
        q, wavelength = self.get_params_(q, wavelength)
        return np.sqrt(np.imag(q)*wavelength/(np.pi))

    @property
    def w0(self):
        """Beam waist `w0` (1/e^2 intensity radius)."""
        return self.get_w0()

    @w0.setter
    def w0(self, w0):
        """
        Set beam waist (1/e^2 intensity radius) to `w0`.
        This changes the imaginary value of the complex beam parameter `q`, but leaves the real part
        unchanged.
        """
        self.q = np.real(self.q) + 1j*np.pi*w0**2/self.wavelength

    def get_w(self, q=None, wavelength=None):
        """
        Calculate beam radius `w` (1/e^2 intensity radius) from complex beam parameter `q`
        and wavelength `wavelength`.
        """
        q, wavelength = self.get_params_(q, wavelength)
        return np.sqrt(-1/np.imag(1/q)*wavelength/np.pi)

    @property
    def w(self):
        """Beam radius `w` (1/e^2 intensity radius)."""
        return self.get_w()

    def get_zr(self, q=None, wavelength=None):
        """
        Calculate Rayleigh range `zr` from complex beam parameter `q`.
        """
        q = self.q if q is None else q
        return np.imag(q)

    @property
    def zr(self):
        """Rayleigh range `zr`."""
        return self.get_zr()        

    def get_z(self, q=None, wavelength=None):
        """
        Calculate position `z` along optical axis relative to beam waist from complex beam
        parameter `q`.
        """
        q = self.q if q is None else q
        return np.real(q)

    @property
    def z(self):
        """position `z` along optical axis relative to beam waist."""
        return self.get_z()             

    def get_inv_roc(self, q=None, wavelength=None):
        """
        Calculate inverse of radius of curvature `roc` from complex beam parameter `q`
        and wavelength `wavelength`.
        """
        q, wavelength = self.get_params_(q, wavelength)        
        if np.real(q) == 0:
            inv_roc = 0
        else:
            inv_roc = np.real(1/q)
        return inv_roc

    @property
    def inv_roc(self):
        """Inverse of radius of curvature `roc`."""
        return self.get_inv_roc()            

    def get_roc(self, q=None, wavelength=None):
        """
        Calculate radius of curvature `roc` from complex beam parameter `q`
        and wavelength `wavelength`.
        """
        inv_roc = self.get_inv_roc(q, wavelength)
        return np.inf if inv_roc == 0 else 1/inv_roc

    @property
    def roc(self):
        """Radius of curvature `roc`."""
        return self.get_roc()                 

    @roc.setter
    def roc(self, roc):
        """
        Set radius of curvature to `roc`.
        This changes the real value of the complex beam parameter `q`, but leaves the imaginary part
        unchanged.
        """
        self.q = 1/(1j*np.imag(1/self.q)+1/roc)

    def get_gouy_phase(self, q=None):
        """Calculate Gouy phase `psi` from complex beam parameter `q`."""
        return np.arctan(self.get_z(q)/self.get_zr(q))

    @property
    def psi(self):
        """Gouy phase `psi`."""
        return self.get_gouy_phase()           

    def laguerre_gauss(self, p, l, r, phi, q=None, wavelength=None):
        """
        Return electric field of Laguerre-Gauss mode with radial mode index `p` and azimuthal mode
        index `l` at polar coordinate (`r`, `phi`) and for complex beam parameter `q` and
        wavelength `wavelength`.
        Normalized such that integral over `r` from 0 to infinity, `phi` from 0 to 2 pi of
        absolute square of electric field is `delta_p,p' * delta_l,l`, where `delta_i,i'` is
        the Kronecker delta.
        """
        q, wavelength = self.get_params_(q, wavelength)
        z = self.get_z(q)
        zr = self.get_zr(q)
        w0 = self.get_w0(q, wavelength)
        w = self.get_w(q, wavelength)
        roc = self.get_roc(q, wavelength)        
        inv_roc = self.get_inv_roc(q, wavelength)                
        psi = self.get_gouy_phase(q)
        return (
            np.sqrt(2/np.pi*(math.factorial(p)/math.factorial(p+np.abs(l))))
            *1/w*(r*np.sqrt(2)/w)**np.abs(l)
            * np.exp(-(r**2/w**2))
            * scipy.special.eval_genlaguerre(p, np.abs(l), (2*r**2)/w**2)
            * np.exp(-1j*2*np.pi/wavelength*r**2/roc/2)
            * np.exp(-1j*l*phi)
            * np.exp(1j*(2*p+np.abs(l)+1)*psi)
            )

    def apply_abcd(self, abcd):
        """Apply ABCD matrix `abcd` (2 x 2 array) to beam."""
        self.q = (abcd[0, 0]*self.q+abcd[0, 1])/(abcd[1, 0]*self.q+abcd[1, 1])    