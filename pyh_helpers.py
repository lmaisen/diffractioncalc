# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

"""

import numpy as np

def getCombinedDataMask(dataFilters, dataFiltersActiveMask, dfAll):

    dataMasks = np.ones((len(dataFilters),len(dfAll)), dtype=bool)
    for iDataFilter, DataFilter in enumerate(dataFilters[dataFiltersActiveMask]):
        DataFilter.builtMaskDfs(dfAll)
        dataMasks[iDataFilter] = DataFilter.dfAllMask
    dataMask = np.all(dataMasks, axis=0)
    return dataMask

class Units():

    unitPrefixes = {
        'y': 1e-24,  # yocto
        'z': 1e-21,  # zepto
        'a': 1e-18,  # atto
        'f': 1e-15,  # femto
        'p': 1e-12,  # pico
        'n': 1e-9,   # nano
        'µ': 1e-6,   # micro
        'm': 1e-3,   # mili
        'c': 1e-2,   # centi
        'd': 1e-1,   # deci
        'k': 1e3,    # kilo
        'M': 1e6,    # mega
        'G': 1e9,    # giga
        'T': 1e12,   # tera
        'P': 1e15,   # peta
        'E': 1e18,   # exa
        'Z': 1e21,   # zetta
        'Y': 1e24,   # yotta
        }

    def __init__(self, prefixExp=1, baseUnit='', digits=0, **kwds):

        super().__init__(**kwds)

        self.invUnitPrefix = {v: k for k, v in self.unitPrefixes.items()}
        self.setUnit(prefixExp, baseUnit)
        self.digits = digits

    def setUnit(self, prefixExp, baseUnit):
        # Find unit name from exponent given in settings
        if prefixExp in self.invUnitPrefix:
            unitPrefix = self.invUnitPrefix[prefixExp]
            unitName = self.invUnitPrefix[prefixExp] + baseUnit
            scaleFactor = 1/prefixExp
        else:
            unitPrefix = ''
            unitName = baseUnit
            scaleFactor = 1
        self.baseUnit = baseUnit
        self.unitPrefix = unitPrefix
        self.scaleFactor = scaleFactor
        self.unitName = unitName

    def getValue(self, value):

        return '{:.{}f}'.format(value*self.scaleFactor,int(self.digits-np.log10(self.scaleFactor)))

    def getStr(self, value):

        return '{:.{}f} {}'.format(value*self.scaleFactor,int(self.digits-np.log10(self.scaleFactor)),self.unitName)
