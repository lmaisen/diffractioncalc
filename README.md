# DiffractionCalc

Diffraction calculation of laser beams and numerical integration of Optical Bloch Equations (OBEs) using these laser fields, used to estimate various shifts in hydrogen spectroscopy caused by beam profile imperfections.

Written by Lothar Maisenbacher for the 2S-nP hydrogen spectroscopy project at MPQ.

## Preparing environment

The custom packages [`pyhs`](https://gitlab.mpcdf.mpg.de/lmaisen/pyhs) and [`pyha`](https://gitlab.mpcdf.mpg.de/lmaisen/pyha) are required. To install these and the remaining dependencies, clone this repository, and run (preferentially in a virtual environment)
```
pip install -r requirements.txt
```