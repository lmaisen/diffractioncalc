# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 13:52:03 2020

@author: Lothar Maisenbacher/MPQ

Class containing definitions and functions pertaining to various electric fields
"""

import numpy as np
import os

import fresnel_func

class Fields:

    def __init__(self, lambda0=None, w0_Fiber=None, f_Coll=None, rMaxInt=None):

        # Dict of electric fields
        self.fields = {}

        # Wavelength
        if lambda0 is None:
            lambda0 = 410e-9
        self.lambda0 = lambda0
        #lambda0 = 380e-9
        #lambda0 = 486e-9

        # Fiber beam waist (MFD/2)
        if w0_Fiber is None:
            w0_Fiber = 3.6e-6/2  # 410 nm
        self.w0_Fiber = w0_Fiber
        #w0_Fiber = 2.5e-6/2  # 380 nm
        #w0_Fiber = 4.6e-6/2  # 486 nm

        # Beam and collimator properties
        if f_Coll is None:
            f_Coll = 30e-3  # Focal length of collimator
        self.f_Coll = f_Coll

        # Upper limit for integration due to optics size
        if rMaxInt is None:
            rMaxInt = 12.5e-3  # Default: 1"
        self.rMaxInt = rMaxInt

        self.update_optical_params()

    def update_optical_params(self):

        self.zR_Fiber = fresnel_func.Gauss_zR(self.w0_Fiber, self.lambda0)
        self.zR_Coll = fresnel_func.lens_zR(self.zR_Fiber, self.f_Coll)
        self.w0_Coll = fresnel_func.Gauss_w0(self.zR_Coll, self.lambda0)

    def get_f_delta_from_delta(self, delta):
        '''
        Get the focal length f_delta of an additional lens between fiber and collimator
        that is equivalent of changing the distance between fiber and collimator
        by length delta.
        Used to change the focussing of a simulated collimated beam profile.
        '''

        f_delta = fresnel_func.get_f_delta_from_delta(
            delta, self.f_Coll, self.zR_Fiber)
        return f_delta

    def make_Ed_delta_Gaussian(self, s):
        '''
        Returns function of analytical electric field of Gaussian beam.
        Need to use this make function, since defining in-loop will cause
        problems.
        '''

        def Ed_delta_Gaussian(rd, delta):
            return (
                fresnel_func.Gauss_E(
                    rd, self.f_Coll+delta, self.zR_Fiber, self.lambda0)
                * fresnel_func.lens_Phase(rd, self.f_Coll, self.lambda0)
                * np.exp(
                    1j*fresnel_func.sphericalAber_Phase(
                        rd, self.zR_Coll, s, self.lambda0)))
        return Ed_delta_Gaussian

    def make_Ed_delta_pop(self, POP_amp_inter, POP_phase_inter):
        '''
        Returns function of interpolated electric field from POP.
        Need to use this make function, since defining in-loop will cause
        problems.
        '''

        # Define electric field with additional lens with f = f_delta
        def Ed_delta_pop(rd, delta):
            if delta == 0:
                return POP_amp_inter(rd) * np.exp(1j*POP_phase_inter(rd))
            else:
                f_delta = self.get_f_delta_from_delta(delta)
                return (
                    POP_amp_inter(rd)
                    * np.exp(1j*POP_phase_inter(rd))
                    * fresnel_func.lens_Phase(rd, f_delta, self.lambda0))
        return Ed_delta_pop

    def load_pops(self):
        '''
        Load POP from file, interpolate, and add as electric field
        '''

        for i_field, (field_id, field) in enumerate(fields.items()):

            if field.get('Source') != 'POP':
                continue

            # Read files and process data
            POP_amp, POP_phase = fresnel_func.readZemaxPOP(field['Filepath'])
            mask = np.abs(POP_amp[:,0]) < self.rMaxInt

            # Interpolate
            POP_amp_inter, POP_phase_inter = (
                fresnel_func.interpolatePOP(POP_amp[mask], POP_phase[mask]))

            fields[field_id] = {
                'EDelta': self.make_Ed_delta_pop(POP_amp_inter, POP_phase_inter),
                'DeltaList': delta_list_0,
                **fields[field_id],
                }

    def add_analytical_fields(self):

        fields = self.fields
        for i_field, (field_id, field) in enumerate(fields.items()):

            if field.get('Source') != 'Analytic':
                continue

            fields[field_id] = {
                **fields[field_id],
                'EDelta': self.make_Ed_delta_Gaussian(
                    field.get('SpherAberS', 0.)),
                }
        self.fields = fields

    def process_fields(self):
        '''
        Add electric field from POP files, analytical definitions, and add
        field at optimal delta if given and for x-y coordinates
        '''

        self.load_pops()
        self.add_analytical_fields()

        def make_Ed(Ed_delta, delta):
            def Ed(rd):
                return Ed_delta(rd, delta)
            return Ed
        def make_Ed_xy(Ed):
            def Ed_xy(x, y):
                return Ed(np.sqrt(x**2+y**2))
            return Ed_xy

        fields = self.fields
        for field_id, field in fields.items():

            fields[field_id]['Delta'] = field.get('Delta', 0.)
            fields[field_id]['E'] = (
                fields[field_id].get('E', make_Ed(field['EDelta'], field['Delta'])))
            fields[field_id]['EXY'] = (
                fields[field_id].get('EXY', make_Ed_xy(field['E'])))
        self.fields = fields

#%% Create default instance

# Wavelength
#lambda0 = 380e-9
lambda0 = 410e-9
#lambda0 = 486e-9

# Fiber beam waist (MFD/2)
#w0_Fiber = 2.5e-6/2  # 380 nm
w0_Fiber = 3.6e-6/2  # 410 nm
#w0_Fiber = 4.6e-6/2  # 486 nm

# Beam and collimator properties
f_Coll = 30e-3  # Focal length of collimator
zR_Fiber = fresnel_func.Gauss_zR(w0_Fiber, lambda0)
zR_Coll = fresnel_func.lens_zR(zR_Fiber, f_Coll)
w0_Coll = fresnel_func.Gauss_w0(zR_Coll, lambda0)

# Geometry
# Distance of HR mirror from collimator
z_Mirror = 0.33
# Distance of interaction region from mirror,
# i.e. first interaction point at dz_Interaction after collimator,
# second interaction point at z_Mirror + dz_Interaction after collimator
dz_Interaction = 0.16

# Integration settings
k = 14  # 2**k+1 integration samples
rdLim = 4*w0_Coll  # Upper integration limit for diffraction integral

# Upper limit for integration due to optics size
rMaxInt = 12.5e-3  # Default: 1/2"

# Number of fiber-collimator distances delta to evaluate to find optimal delta
num_deltas = 20

myFields = Fields(
    lambda0=lambda0, w0_Fiber=w0_Fiber, f_Coll=f_Coll, rMaxInt=rMaxInt)

# Dict of electric fields
fields = myFields.fields

#%% Read ZEMAX POP (physical optics propagation),
# i.e. simulation of electric field from collimator

# Input files
pop_folder = 'ZEMAXPOP'

delta_list_0 = np.linspace(-5e-6, 6e-6, num_deltas)

# B. Halle 3 lens collimator 2016
filename = 'BHa_410nm_MFD3.6um_2048auto'
fields[filename] = {
    'Source': 'POP',
    'Filepath': os.path.join(pop_folder, filename),
    'Delta': -1.08132604e-05,
    'DeltaList': np.linspace(-13e-6, -7e-6, num_deltas),
    }

# B. Halle 4 lens final design V11 14.09.2017
filename = 'BHaV11_410nm_MFD3.6um_2048auto'
fields[filename] = {
    'Source': 'POP',
    'Filepath': os.path.join(pop_folder, filename),
    'Delta': 8.733867566234533e-07,
    'DeltaList': delta_list_0,
    }
#filename = 'BHaV11_380nm_MFD2.5um_2048auto'
#filename = 'BHaV11_486nm_MFD4.6um_2048auto'
# Toleranced version 18.09.2017
#filename = 'BHaV11Tol1_410nm_MFD3.6um_2048auto_x'
#filename = 'BHaV11Tol1_410nm_MFD3.6um_2048auto_y'
# Misalignments
#filename = 'BHaV11_Mis_0' # 9.99922478e-01
#filename = 'BHaV11_Mis_L1_1deg' # Lens 1 tilted about y axis by 1 deg, backcoupling: 9.96327068e-01
# Lens 3 tilted about y axis by 1 deg, backcoupling: 9.99885527e-01
filename = 'BHaV11_Mis_L3_1deg'
fields[filename] = {
    'Source': 'POP',
    'Filepath': os.path.join(pop_folder, filename),
    'Delta': -1.4520876798527736e-06,
    'DeltaList': delta_list_0,
    }

#%% Define beam after collimator analytically

## Analytic definition of input electric field
# Field in plane of collimator

# No aberrations
delta_list_0 = np.linspace(-1e-6, 1e-6, num_deltas)
fields['Gaussian_s=0'] = {
    'Source': 'Analytic',
    'SpherAberS': 0.,
    'Delta': 0.18e-6,
    'DeltaList': delta_list_0,
    }

# Spherical aberrations
s = -0.3  # 410 nm
delta_list_0 = np.linspace(-5e-6, 5e-6, num_deltas)
fields['Gaussian_s=-0.3'] = {
    'Source': 'Analytic',
    'SpherAberS': s,
    'Delta': -2.13686974e-06,
    'DeltaList': delta_list_0,
    }

myFields.fields = fields
myFields.process_fields()
