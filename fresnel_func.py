# -*- coding: utf-8 -*-
"""
Created on Thu May 18 00:38:03 2017

@author: Lothar Maisenbacher/MPQ

Collection of functions related to diffraction analysis
"""

import numpy as np
import scipy.integrate
import scipy.special
import scipy.optimize
import scipy.interpolate
import poppy

# Gaussian beam propagation and parameters.
def Gauss_zR(w0, lambda0):
    return np.pi*w0**2/lambda0

def Gauss_w0(zR, lambda0):
    return np.sqrt(zR*lambda0/np.pi)

def Gauss_w(z, zR, lambda0):
    return Gauss_w0(zR, lambda0)*np.sqrt(1+(z/zR)**2)

def Gauss_R(z, zR):
    if z == 0:
        return np.inf
    else:
        return z*(1+(zR/z)**2)

def GouyPhase(z, zR):
    return np.arctan(z/zR)

def Gauss_E(r, z, zR, lambda0):
    k0 = 2*np.pi/lambda0
    return (
        Gauss_w0(zR, lambda0)
        /Gauss_w(z, zR, lambda0)
        * np.exp(-r**2/Gauss_w(z, zR, lambda0)**2)
        * np.exp(-1j*k0*r**2/2/Gauss_R(z, zR))
        * np.exp(1j*GouyPhase(z, zR))
        * np.exp(-1j*k0*z)
        )

def FresnelInt(r, rd, z, lambda0):
    """
    Fresnel approx. to Huygens' integral kernel
    (see Siegmann, Lasers, Ch. 16, Eq. (18)).
    In cylindrical coordinates for rotationally symmetry input field.
    Gives the electric field at point (r, z), with r radial position and z object
    distance, originating from point (rd, 0), for wavelength lambda0.
    """
    k0 = 2*np.pi/lambda0
    return (
        1j*2*np.pi/(z*lambda0)
        * np.exp(-1j*k0*z)
        * np.exp(-1j*k0*r**2/2/z)
        * rd
        * np.exp(-1j*k0*rd**2/2/z)
        * scipy.special.jv(0, k0*r*rd/z)
        )

def lens_Phase(r, f, lambda0):
    """Phase mask for a lens with focal length `f`."""
    k0 = 2*np.pi/lambda0
    return np.exp(1j*r**2*k0/2/f)

def lens_zR(zRin, f):
    """
    Rayleigh length after a lens with focal length `f` and a input beam with ("collimated")
    Rayleigh length `zRin`.
    """
    return f**2/zRin

def sphericalAber_Phase(r, zR, s, lambda0):
    """
    Phase mask representing spherical aberration with strength `s`,
    i.e., a phase quartic in `r` with strength of `s` rad at `r = w` is added.
    A term quadratic in `r` compensates for the focusing of the r^4 term.
    """
    return s*(r**4/Gauss_w0(zR, lambda0)**4 - 2*r**2/Gauss_w0(zR, lambda0)**2)

def calcFresnel(Ed, Ed_lim, r, z, lambda0):
    """
    Calculate Fresnel integral given in FresnelInt of rotationally symmetric electric field Ed
    to find diffraction pattern at (r, z).
    """
    # Split into two integrals for real and imaginary part
    DiffIntReal = lambda rd, r: np.real(FresnelInt(r, rd, z, lambda0)*Ed(rd))
    DiffIntImag = lambda rd, r: np.imag(FresnelInt(r, rd, z, lambda0)*Ed(rd))

    DiffE = np.zeros(len(r), dtype=np.complex)
    for i, ra in enumerate(r):
        a = scipy.integrate.quad(
            lambda rd: DiffIntReal(rd, ra), 0, Ed_lim, epsabs=1e-10, epsrel=1e-10, limit=10000)[0]
        b = scipy.integrate.quad(
            lambda rd: DiffIntImag(rd, ra), 0, Ed_lim, epsabs=1e-10, epsrel=1e-10, limit=10000)[0]
        DiffE[i] = a+1j*b

    return DiffE

def calcFresnelDisc(Ed, Ed_lim, r, z, k, lambda0):
    """
    Discrete evaluation of Fresnel integral,
    where the integral in question is first evaluated at 2**k+1 equally-spaced points
    and then integrated using scipy.integrate.romb.
    Much fast than direct integration, I suspect because of the
    rapidly varying nature of the integral.
    """
    # Split into two integrals for real and imaginary part
    DiffIntReal = lambda rd, r: np.real(FresnelInt(r, rd, z, lambda0)*Ed(rd))
    DiffIntImag = lambda rd, r: np.imag(FresnelInt(r, rd, z, lambda0)*Ed(rd))

    DiffE = np.zeros(len(r), dtype=np.complex)
    for i, ra in enumerate(r):
        a = scipy.integrate.romb(DiffIntReal(np.linspace(0, Ed_lim, 2**k+1), ra),Ed_lim/(2**k))
        b = scipy.integrate.romb(DiffIntImag(np.linspace(0, Ed_lim, 2**k+1), ra),Ed_lim/(2**k))
        DiffE[i] = a+1j*b

    return DiffE

def calcFresnelDiscE(rd,Ed,r,z,lambda0):
    """
    Discrete integration for an E field given at discrete sampled points, e.g., from ZEMAX POP.
    Note however that a large number of points is needed to give satisfactory results, which might
    not be available. In this case, one can interpolate the electric field first,
    granted that it is sufficiently smooth, and then integrate this function.
    """
    DiffIntReal = lambda r: np.real(FresnelInt(r,rd,z,lambda0)*Ed)
    DiffIntImag = lambda r: np.imag(FresnelInt(r,rd,z,lambda0)*Ed)

    DiffE = np.zeros(len(r), dtype=np.complex)
    for i, ra in enumerate(r):
        a = scipy.integrate.trapz(DiffIntReal(ra),rd)
        b = scipy.integrate.trapz(DiffIntImag(ra),rd)
        DiffE[i] = a+1j*b

    return DiffE

def calcOverlap(r, E1, E2):
    """
    Calculate overlap integral between circularly-symmetric electric fields E1 and E2,
    defined at radii `r`.
    """
    return (
        np.abs(scipy.integrate.trapz(
            r*(E1*np.conjugate(E2)), r))**2
        /(
            np.real(scipy.integrate.trapz(r*(E1*np.conjugate(E1)), r))
            * np.real(scipy.integrate.trapz(r*(E2*np.conjugate(E2)), r))
        )
        )

def calcIntCont(r, E1):
    """
    Calculate power contained in circularly-symmetric electric field `E1`,
    defined at radii `r`.
    """
    return 2*np.pi*np.real(scipy.integrate.trapz(r*(E1*np.conjugate(E1)), r))

def interEr(r, DiffEr):
    """Interpolate electric fields for OBE."""
    DiffEr_inter_r = scipy.interpolate.interp1d(
        r, np.real(DiffEr), kind='linear', fill_value=[0], bounds_error=False)
    DiffEr_inter_i = scipy.interpolate.interp1d(
        r, np.imag(DiffEr), kind='linear', fill_value=[0], bounds_error=False)
    return lambda r: DiffEr_inter_r(r) + 1j*DiffEr_inter_i(r)

def rInt(r, rMaxInt):
    """
    Limit evaluation to a maximum radius (e.g., clear aperture of lenses),
    corresponding to a hard edge.
    """
    if r < rMaxInt:
        rMax = r
    else:
        rMax = rMaxInt
        print('Integration radius limited to {:.2e} m instead of {:.2e} m'.format(rMaxInt, r))
    return rMax

def ZernRad(n,rho):
    """Zernike polynomials."""
    rho = np.array([rho]).flatten()
    atEdge = poppy.zernike.zernike(n, 0, rho=np.array([1]), theta=np.array([0]))
    return poppy.zernike.zernike(n, 0, rho=rho, theta=np.zeros(len(rho)), outside=atEdge)

def readZemaxPOP(filepath):
    """Load Zemax POP (physical optics propagation) results from files."""
    POP_int = np.genfromtxt(filepath + '_Int.txt', skip_header=13, dtype=float)
    POP_int[:,0] = 1e-3*POP_int[:,0]
    POP_amp = POP_int.copy()
    POP_amp[:,1] = np.sqrt(POP_amp[:,1])
    POP_phase = np.genfromtxt(filepath + '_Phase.txt', skip_header=13, dtype=float)
    POP_phase[:,0] = 1e-3*POP_phase[:,0]
    POP_phase[:,1] = np.unwrap(POP_phase[:,1])

    return POP_amp, POP_phase

def interpolatePOP(POP_amp, POP_phase):
    """Interpolate ZEMAX POP (physical optics propagation) results."""
    POP_amp_inter = scipy.interpolate.interp1d(
        POP_amp[:,0], POP_amp[:,1], kind='cubic', fill_value=[0], bounds_error=False)
    POP_phase_inter = scipy.interpolate.interp1d(
        POP_phase[:,0], POP_phase[:,1], kind='cubic', fill_value=[0], bounds_error=False)

    return POP_amp_inter, POP_phase_inter

def get_f_delta_from_delta(delta, f_Coll, zR_Fiber):
    """
    Get the focal length `f_delta` of an additional lens between fiber and collimator
    that is equivalent of changing the distance between fiber and collimator
    by length `delta`.
    Used to change the focussing of a simulated collimated beam profile.
    """
    f_delta = (
        (1/(2*zR_Fiber**2*delta))
        * (
           f_Coll**2*zR_Fiber**2 - zR_Fiber**4 - 2*f_Coll*zR_Fiber**2*delta
           + f_Coll**2*delta**2 - zR_Fiber**2*delta**2
           + np.sqrt(
               f_Coll**4*zR_Fiber**4 - 2*f_Coll**2*zR_Fiber**6
               + zR_Fiber**8 - 8*f_Coll**3*zR_Fiber**4*delta
               - 2*f_Coll**4*zR_Fiber**2*delta**2
               - 4*f_Coll**2*zR_Fiber**4*delta**2
               + 2*zR_Fiber**6*delta**2 - 8*f_Coll**3*zR_Fiber**2*delta**3
               + f_Coll**4*delta**4
               - 2*f_Coll**2*zR_Fiber**2*delta**4 + zR_Fiber**4*delta**4)))

    return f_delta

def get_f_delta_from_delta_approx(delta, f_Coll, zR_Fiber):
    """
    Approximation to `get_f_delta_from_delta`.
    """
    optical_power = (
        (f_Coll**2*zR_Fiber**2
         - f_Coll**2*zR_Fiber*np.sqrt(zR_Fiber**2-4*delta**2))
        /(2*f_Coll**4*delta))
    f_delta = 1/optical_power

    return f_delta

def get_delta_from_f_delta(f_delta, f_Coll, zR_Fiber):
    """
    Get the distance change `delta` between fiber and collimator
    that is equivalent of an additional lens with focal length `f_delta` between
    fiber and collimator.
    """
    delta = (
        (
            f_Coll**4 + f_Coll**2*zR_Fiber**2 + 2*f_Coll*f_delta*zR_Fiber**2
            + f_delta**2*zR_Fiber**2
            - np.sqrt(
                (-f_Coll**4 - f_Coll**2*zR_Fiber**2 - 2*f_Coll*f_delta*zR_Fiber**2 -
                 f_delta**2*zR_Fiber**2)**2
                - (
                    4*(-f_Coll**3 + f_Coll**2*f_delta - f_Coll*zR_Fiber**2 - f_delta*zR_Fiber**2)
                    *(-f_Coll**3*zR_Fiber**2 + f_Coll**2*f_delta*zR_Fiber**2 - f_Coll*zR_Fiber**4 - f_delta*zR_Fiber**4)
                ))
        )
        / (2*(-f_Coll**3 + f_Coll**2*f_delta - f_Coll*zR_Fiber**2 - f_delta*zR_Fiber**2)))

    return delta

def get_delta_from_f_delta_approx(f_delta, f_Coll, zR_Fiber):
    """Approximation to `get_delta_from_f_delta`."""
    optical_power = 1/f_delta
    delta = (
        (f_Coll**2 * zR_Fiber**2/optical_power)
        /(f_Coll**4 + zR_Fiber**2/optical_power**2))

    return delta
