# -*- coding: utf-8 -*-
"""
Created on Thu May 18 09:01:59 2017

@author: Lothar Maisenbacher/MPQ

Plot results of OBE calculation of Doppler shift
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec
import datetime

# pyh
import pyh_helpers

# pyhs
import pyhs.fit
import pyhs.fitfunc

myFit = pyhs.fit.Fit()

import pyha.plts as plts
plts.set_params()

#%% Open figure

fig_results_obe = plt.figure(
    'OBE results', figsize=(plts.pagewidth, 5), constrained_layout=True)
fig_results_obe.clf()

plot_params = {
    'ShowDopplerShiftFromIntMismatch': False,
    'AlphaOffsetExample': -4.,
#    'XLim': [-17, -8],
    }

#%% Load OBE results

input_dir = 'obe_results'
filename_prefixes = [
#    '2020-08-28 02-24-02 x7YVNTTv Gaussian_s=0',
#    '2020-08-28 03-34-46 r3bizjJn Gaussian_s=-0.3',
#    '2020-10-08 06-42-32 4LGdqeYk BHa_410nm_MFD3.6um_2048auto',
    '2020-10-08 04-33-45 xcY6cW0s BHaV11_410nm_MFD3.6um_2048auto',
    ]
resultsOBE = pd.DataFrame()
for i, filename_prefix in enumerate(filename_prefixes):
    resultsOBE_ = pd.read_csv(
        os.path.join(input_dir, filename_prefix + ' resultsOBE.dat'),
        index_col=0)
    resultsOBE = resultsOBE.append(resultsOBE_, ignore_index=True)
    if i == 0:
        scan_detunings = np.load(
            os.path.join(input_dir, filename_prefix + ' detunings.npy'))
        scan_results = np.load(
            os.path.join(input_dir, filename_prefix + ' scanresults.npy'))
    else:
        scan_detunings_ = np.load(
            os.path.join(input_dir, filename_prefix + ' detunings.npy'))
        scan_detunings = np.vstack((scan_detunings, scan_detunings_))
        scan_results_ = np.load(
            os.path.join(input_dir, filename_prefix + ' scanresults.npy'))
        scan_results = np.vstack((scan_results, scan_results_))

#%% Plot preparation

lambda0 = resultsOBE['Lambda'].mode()[0]

# Unit for delta
delta_unit = pyh_helpers.Units(
    1e-6,
    baseUnit='m')

#%% Plot results

# Prepare figure
fig_results_obe.clf()
gs = matplotlib.gridspec.GridSpec(
    3, 1,
    width_ratios=[1],
    height_ratios=[1, 1, 2],
    figure=fig_results_obe,
    )
ax1 = fig_results_obe.add_subplot(gs[0])
ax2 = fig_results_obe.add_subplot(gs[1], sharex=ax1)
ax3 = fig_results_obe.add_subplot(gs[2], sharex=ax1)

colors = plts.get_line_colors(cmap='tab10')

mask = (
    pd.Series(True, index=resultsOBE.index)
    )

fit_func_lin = myFit.fit_funcs['Linear']
fit_func_para = myFit.fit_funcs['Parabola']

alpha_offsets = resultsOBE[mask]['AlphaOffset'].unique()
lin_fits_vx = np.zeros((len(alpha_offsets), 2))
colors_tv = plts.get_line_colors(len(alpha_offsets), cmap='tab20')
for i, alpha_offset in enumerate(alpha_offsets):

    mask2 = mask & (resultsOBE['AlphaOffset'] == alpha_offset)

    deltas = resultsOBE[mask2]['Delta'].unique()
    y_mean = np.zeros(len(deltas))
    y_sigma = np.zeros(len(deltas))
    for j, delta in enumerate(deltas):
        mask3 = mask2 & (resultsOBE['Delta'] == delta)
        y_mean[j] = np.mean(resultsOBE[mask3]['CFR_Value'].values)
        y_sigma[j] = np.std(resultsOBE[mask3]['CFR_Value'].values)

    y = y_mean
    if not np.all(np.isnan(y)):
        ax3.errorbar(
            deltas * delta_unit.scaleFactor, y,
            y_sigma,
            **plts.get_errorbar_params(**{
                'markersize': 2,
                'label': f'{alpha_offset:.2f}',
                'linestyle': '',
                'color': colors_tv[i],
                }))

    pstart_lin = [(y[-1]-y[0])/np.ptp(deltas), 0]
    fit_result_lin, error_msg = myFit.doFit(
        fit_func_lin,
        resultsOBE[mask2]['Delta'],
        resultsOBE[mask2]['CFR_Value'],
        pstart=pstart_lin)
    lin_fits_vx[i] = fit_result_lin['Popt']
    ax3.plot(
        deltas * delta_unit.scaleFactor,
        fit_func_lin['Func'](deltas, *lin_fits_vx[i]),
        **plts.get_line_params(**{
            'linestyle': ':',
            'color': colors_tv[i],
            }))

if plot_params.get('XLim') is not None:
    ax3.set_xlim(plot_params['XLim'])

ax3_ylim = ax3.get_ylim()
ax3_xlim = ax3.get_xlim()
ax3.plot(
    ax3_xlim, [0, 0],
    **plts.get_line_params(**{
        'linestyle': '--', 'color': '0.5',
        }))
ax3.set_xlim(ax3_xlim)

## Plot backcoupling into fiber
deltas = resultsOBE[mask]['Delta'].values
y = resultsOBE[mask]['BeamOverlap'].values

# Fit with quadratic function
pstart_para = [-1e9, np.mean(deltas), np.max(y)]
x_fit_para = np.linspace(
    np.min(deltas), np.max(deltas), 1000)
fit_result_para, error_msg = myFit.doFit(
    fit_func_para,
    deltas, y, pstart=pstart_para)
sr_fit_para = myFit.fit_result_to_dict(
    fit_result_para, fit_func_para)
ax1.plot(
    x_fit_para * delta_unit.scaleFactor,
    fit_func_para['Func'](
        x_fit_para, *fit_result_para['Popt']),
    **plts.get_line_params(**{
        'color': colors[3],
        }))
min_backcoupl = 0.99
popt = fit_result_para['Popt']
delta0_backcoupling = sr_fit_para['Center_Value']
delta1_backcoupling = -np.sqrt(popt[2]*(min_backcoupl-1)/popt[0])+popt[1]
delta2_backcoupling = np.sqrt(popt[2]*(min_backcoupl-1)/popt[0])+popt[1]

# Add vertical markers
ax1_ylim = ax1.get_ylim()
ax1_delta0_backcoupling = ax1.plot(
    np.tile(delta0_backcoupling * delta_unit.scaleFactor, 2), ax1_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': colors[3],
        'label': (
            r'$1-P^{}_\mathrm{{bc,max}}$'
            +f' = {(1-sr_fit_para["Offset_Value"]):.2e}'
            ),
        }))
ax1_delta1_backcoupling = ax1.plot(
    np.tile(delta1_backcoupling * delta_unit.scaleFactor, 2), ax1_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': colors[0],
        'label': f'{100*min_backcoupl:.0f}% '+r'$P^{}_\mathrm{{bc,max}}$',
        }))
ax1_delta2_backcoupling = ax1.plot(
    np.tile(delta2_backcoupling * delta_unit.scaleFactor, 2), ax1_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': colors[0],
        }))
ax3_delta0_backcoupling = ax3.plot(
    np.tile(delta0_backcoupling * delta_unit.scaleFactor, 2), ax3_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': colors[3],
        }))
ax3_delta1_backcoupling = ax3.plot(
    np.tile(delta1_backcoupling * delta_unit.scaleFactor, 2), ax3_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': colors[0],
        }))
ax3_delta2_backcoupling = ax3.plot(
    np.tile(delta2_backcoupling * delta_unit.scaleFactor, 2), ax3_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': colors[0],
        }))
ax1.set_ylim(ax1_ylim)

# Plot data
ax1.plot(
    deltas * delta_unit.scaleFactor, y,
    **plts.get_point_params(**{
        'color': colors[0],
        }))

## Plot intensity mismatch xi
# Intensity mismatch xi is
# (1 - ratio of center intensity of backward- to forward-traveling beam)
int_mismatch = resultsOBE[mask]['IntMismatch'].values

# Plot data
ax2.plot(
    deltas * delta_unit.scaleFactor, int_mismatch,
    **plts.get_point_params(**{
        'color': colors[0],
        }))

# Fit with linear function
pstart_lin = [-1, 0]
fit_result_lin, error_msg = myFit.doFit(
    fit_func_lin,
    deltas, int_mismatch, pstart=pstart_lin)
delta0_int_mismatch, delta0_sigma_int_mismatch = pyhs.fitfunc.Linear_x0(
    fit_result_lin['Popt'], fit_result_lin['Pcov'])
sr_fit_int_mismatch = myFit.fit_result_to_dict(
    fit_result_lin, fit_func_lin)
deltas_fit = np.linspace(np.min(deltas), np.max(deltas), 1000)
ax2.plot(
    deltas_fit * delta_unit.scaleFactor,
    fit_func_lin['Func'](deltas_fit, *fit_result_lin['Popt']),
    **plts.get_line_params(**{
        'color': '0.5',
        }))

# Add vertical markers
ax2_ylim = ax2.get_ylim()
ax2_x0 = ax2.plot(
    np.tile(delta0_int_mismatch * delta_unit.scaleFactor, 2), ax2_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': '0.5',
        'label': r'$\xi^{}_\mathrm{cent}$ = 0',
        }))
ax3_delta0_int_mismatch = ax3.plot(
    np.tile(delta0_int_mismatch * delta_unit.scaleFactor, 2), ax3_ylim,
    **plts.get_line_params(**{
        'linestyle': '--', 'color': '0.5',
        }))
ax2.set_ylim(ax2_ylim)

## Finish plot
# Add Doppler shift for example transverse velocity
#vx_example = transverse_vels[0]
alpha_offset_example = plot_params.get('AlphaOffsetExample', -4.)
mask_vx_example = (resultsOBE['AlphaOffset'] == alpha_offset_example)
if mask_vx_example.sum() > 0:
    i_example = np.argmax(alpha_offsets == alpha_offset_example)
    resultsOBE_vx_example = resultsOBE[mask_vx_example].iloc[0]
    def pick_vx_example(delta):

        yd = fit_func_lin['Func'](delta, *lin_fits_vx[i_example])
        retstr = '{:.0f} Hz'.format(yd)

#        deltas = resultsOBE[mask]['Delta'].values
#        norm_deltas = np.abs(
#            (delta-np.unique(deltas))/(delta1_backcoupling-delta0_backcoupling))
    #    if np.all(norm_deltas > 0.05):
#        if True:
    #        retstr = '{:.0f} Hz'.format(yd)
    #    else:
#            deltad = np.unique(deltas)[np.argmin(norm_deltas)]
#            vxdeltadMask = (
#                mask
#                & mask_vx_example
#                & (resultsOBE['Delta'] == deltad)
#                )
#    #        yd = np.mean(resultsOBE[vxdeltadMask]['CFR_Value'].values)
#            if np.sum(vxdeltadMask) > 1:
#                ydSigma = np.std(resultsOBE[vxdeltadMask]['Res_fitFunc_CFR'].values)
#                retstr = r'({:.0f} $\pm$ {:.0f}) Hz'.format(yd, ydSigma)
#            else:
#                retstr = '{:.0f} Hz'.format(yd)
        return retstr
else:
    def pick_vx_example(_):

        return np.nan

if plot_params.get('ShowDopplerShiftFromIntMismatch', False):
    # Show Doppler resulting from intensity mismatch at example transverse velocity
    # Note that this is the intensity mismatch at the center of the beam,
    # while the Doppler shift is accumulated as the atom traverses the beam
    doppler_shift_int_mismatch = (
        resultsOBE_vx_example['V']/resultsOBE[mask_vx_example]['Lambda']/2
        * resultsOBE[mask_vx_example]['AlphaOffset']*1e-3
        * resultsOBE[mask_vx_example]['IntMismatch']
        )
    ax3.plot(
        resultsOBE[mask_vx_example]['Delta'] * delta_unit.scaleFactor,
        doppler_shift_int_mismatch,
        **plts.get_point_params(**{
            'linestyle': '-.', 'color': colors[i_example], 'alpha': 0.7,
            'label': (
                f'{alpha_offset_example:.2f}'
                +'\n'+r'($\xi$ est.)'
                ),
            }))

# Add legends
leg_params = plts.get_leg_params(**{
    'fontsize': plts.params['FontsizeMed'],
    })
ax1.legend(**leg_params)
ax2.legend(**leg_params)
hleg3a = ax3.legend(
    **{
        **leg_params,
        'ncol': 3,
        })
hleg3a.set_title(
    r'$\delta\alpha$ (mrad)',
    prop=leg_params.get('prop'),
    )
ax3.add_artist(hleg3a)

if mask_vx_example.sum() > 0:
    hleg3b = ax3.legend(
        [ax3_delta0_backcoupling[0],
         ax3_delta1_backcoupling[0],
         ax3_delta2_backcoupling[0],
         ax3_delta0_int_mismatch[0],
         ],
        [pick_vx_example(delta0_backcoupling),
         pick_vx_example(delta1_backcoupling),
         pick_vx_example(delta2_backcoupling),
         pick_vx_example(delta0_int_mismatch),
         ],
        **{
            **leg_params,
            'loc': 'lower left',
            })
    hleg3b.set_title(
        r'$\Delta\nu^{}_{\mathrm{D}}$ for $\delta\alpha$ = '
        + r'{:.1f} mrad'.format(
            resultsOBE[mask_vx_example]['AlphaOffset'].iloc[0]),
        prop=leg_params.get('prop'),
        )
    ax3.add_artist(hleg3b)
ax3.set_ylim(ax3_ylim)

#ax1.set_title(resultsOBE['sessionName'].unique()[0])
#ax1.set_title(inputFilename[11:])

ax1.set_ylabel(
    'Backcoupling\ninto fiber '+r'$P^{}_\mathrm{bc}$')
ax2.set_ylabel(
    'Intensity mismatch \n' + r' $\xi^{}_\mathrm{cent} = 1-I_2/I_1$')
ax3.set_ylabel(r'Doppler shift $\Delta\nu^{}_\mathrm{D}$ (Hz)')

ax3.set_xlabel(r'$\delta d^{}_\mathrm{fc}$'+f' ({delta_unit.unitName})')

plt.savefig(os.path.join(
    'obe_plots',
    (
     datetime.datetime.utcnow().strftime('%Y-%m-%d')
     + ' ' + ', '.join(filename_prefixes) + '.pdf')))
