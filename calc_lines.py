# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 00:09:17 2016

@author: Lothar Maisenbacher/MPQ

Numerical integration of OBEs of hydrogen 2S-nP system for given parameters,
using various electric fields as input
"""

import time
import scipy.integrate
import scipy.optimize
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.constants
import itertools
import os

# pyhs
import pyhs.fit
import pyhs._gen

import cOBE_2SnP as OBE_2SnP
import fresnel_func
import diff_fields

myFit = pyhs.fit.Fit()

import pyha.plts as plts
plts.set_params()

#%% Load electric fields

myFields = diff_fields.myFields
myFields.process_fields()
fields = myFields.fields

# Sampling of diffracted pattern
Npoints = 200  # Number of points to calculate
rs = np.linspace(0, 3*myFields.w0_Coll, Npoints)

#%% Select electric field

#field_id = 'Gaussian_s=0'
#field_id = 'Gaussian_s=-0.3'
#field_id = 'BHaV11_Mis_L3_1deg'
field_id = 'BHaV11_410nm_MFD3.6um_2048auto'
#field_id = 'BHa_410nm_MFD3.6um_2048auto'

session_name = field_id

field = fields[field_id]
Ed = field['E']
Ed_xy = field['EXY']
Ed_delta = field['EDelta']
delta_list_0 = field['DeltaList']

#%%

# Init variables
resultsOBE = pd.DataFrame()

# Define atomic system
upper_level_n = 6
myOBE = OBE_2SnP.OBE_2SnP(n=upper_level_n)
myOBE.w0 = 2.2e-3
vr = scipy.constants.hbar * myOBE.k/myOBE.m

fit_func_id = 'Voigt'

# ODE solver method
#ode_solver_method = 'Radau'
ode_solver_method = 'LSODA'

# Select OBE
obe_id = 'derivsArbRabi'

modelArthur = 'C'

## Input state
input_state = 'PlaneWave'

# Set detunings to calculate
detunings = np.linspace(-30e6, 30e6, 61)
#detunings = np.linspace(-50e6, 50e6, 101)
# As used on cluster with C++ version, with -64 MHz point removed
#detunings = np.arange(-63.5e6, 64e6, 0.5e6)
#detunings = np.arange(-64e6, 64.5e6, 1e6)

## Parameters to sweep
laser_powers = [10]
vs = [200]
# Relative backcoupling
p_rels = np.array([0.98, 0.99, 0.995, 0.9987])
#p_rels = np.array([0.99])

#transverse_velocities_vr = [1]
#transverse_velocities_vr = np.arange(0, 2, 0.1)
#alphas = [np.arcsin(vt*vr/vs[0])*1e3 for vt in transverse_velocities_vr]
# Alpha offset angles (mrad)
#alphas = [-4]
alphas = np.linspace(-10, 10, 11)

# Find optimal backcoupling with least square optimization
rsf = np.linspace(
    0,
    fresnel_func.rInt(
        3*fresnel_func.Gauss_w0(myFields.zR_Coll, myFields.lambda0),
        myFields.rMaxInt),
    100)
rIntDiff = fresnel_func.rInt(
    4*fresnel_func.Gauss_w0(myFields.zR_Coll, myFields.lambda0),
    myFields.rMaxInt)
def overlapDelta(deltas):
    deltas = np.array([deltas]).flatten()
    DiffEs = np.array([
        fresnel_func.calcFresnelDisc(
            lambda rd: Ed_delta(rd, delta),
            rIntDiff, rsf, diff_fields.z_Mirror, 14, myFields.lambda0)
        for delta in deltas])
    return np.array([
        fresnel_func.calcOverlap(rsf, DiffE, np.conjugate(DiffE)) for DiffE in DiffEs])

fitTol = 1e-3
startTime = time.time()
poptLS, cov_x, infodict, mesg, ier = scipy.optimize.leastsq(
    lambda delta: overlapDelta(delta)-1, 0, full_output=True, xtol=fitTol)
delta0 = poptLS[0]
overlap0 = overlapDelta(delta0)[0]
print(
    'Maximum backcoupling of {:.10f} found for delta = {:.3e} m'.format(overlap0, delta0))

# Find points on which backcoupling is reduced to a given level
delta_opt_range = np.ptp(field['DeltaList'])
delta_opt_range = 6e-6
delta_list = np.linspace(
    -delta_opt_range+delta0, delta_opt_range+delta0, 10)
backcoupling = overlapDelta(delta_list)

# Fit with quadratic function
fitQu = lambda x, a, x0, y0: a*(x-x0)**2 + y0
pstart = [-1e9, delta0, backcoupling.max()]
popt, pcov = scipy.optimize.curve_fit(
    fitQu, delta_list, backcoupling, pstart)

# Plot result
fig = plt.figure(
    'Overlap integral vs delta', figsize=(5, 5), constrained_layout=True)
plt.clf()
ax1 = plt.gca()
ax1.cla()
ax1.plot(
    delta_list, backcoupling,
    **plts.get_point_params(**{'markersize': 4.}))
x_fit = np.linspace(min(delta_list), max(delta_list), 1000)
#ax1.plot(
#    x_fit, fitQu(x_fit, *popt), linestyle=':')

# Find deltas for relative backcouplings
backcoupling_inter = scipy.interpolate.interp1d(
    delta_list, backcoupling,
    kind='quadratic', fill_value='extrapolate')

deltas_qu = -np.sqrt((p_rels*overlap0 - popt[2])/popt[0])
deltas_qu_both = np.hstack((
    deltas_qu+popt[1],
    -deltas_qu+popt[1],
    ))
ax1.plot(
    deltas_qu_both, backcoupling_inter(deltas_qu_both),
    **plts.get_point_params(**{'marker': 'd', 'markersize': 4.}))

#ax1.plot(
#    x_fit, backcoupling_inter(x_fit), linestyle='--')
deltas_inter_left = np.array([
    scipy.optimize.fsolve(
        lambda x: backcoupling_inter(x)-p_rel*overlap0, deltas_qu+popt[1])[0]
    for p_rel, delta_qu in zip(p_rels, deltas_qu)])
deltas_inter_right = np.array([
    scipy.optimize.fsolve(
        lambda x: backcoupling_inter(x)-p_rel*overlap0, -deltas_qu+popt[1])[0]
    for p_rel, delta_qu in zip(p_rels, deltas_qu)])
deltas_inter_both = np.hstack((
    deltas_inter_left, deltas_inter_right))
ax1.plot(
    deltas_inter_left, backcoupling_inter(deltas_inter_left),
    marker='x', linestyle='')
ax1.plot(
    deltas_inter_right, backcoupling_inter(deltas_inter_right),
    marker='x', linestyle='')

#check_deltas_inter = overlapDelta(deltas_inter_both)
#check_deltas_qu = overlapDelta(deltas_qu_both)

deltas_ = deltas_inter_both
deltas = np.hstack((
    delta0, deltas_))
deltas.sort()
ax1.plot(
    deltas, backcoupling_inter(deltas),
    marker='s', linestyle='')

#%% Calc lines

sweepList = list(itertools.product(
    laser_powers,
    vs,
    alphas,
    deltas,
    ))
scan_detunings = [np.array([])] * len(sweepList)
scan_results = [np.array([])] * len(sweepList)

fit_line = True

# Assign UID
obe_calc_uid = pyhs.gen.get_uid()

for i, (laser_power, v, alpha_offset, delta) \
        in enumerate(sweepList):

    vz = v * np.cos(alpha_offset*1e-3)
    transverse_velocity = v * np.sin(alpha_offset*1e-3)
    n_offset = transverse_velocity/vr
    dpd1 = np.nan

    print(
        'Calculating line {:d} out of {:d}'
        .format(i+1, len(sweepList)))
    print(
        f'Laser power: {laser_power:.1f} µW'
        f'\nAlpha offset: {alpha_offset:.1f} mrad'
        f'\nRecoil offset: {n_offset:.3f}'
        f'\nTransverse velocity: {transverse_velocity:.3f} m/s'
        f'\nDelta p for 1st decay: {dpd1:.3f} hbar k'
        )

    scan_detunings[i] = detunings

    # Set parameters in OBE
    myOBE.velVector = np.array([vz, 0, transverse_velocity], dtype=float)
    myOBE.dpd1 = dpd1

    if modelArthur == 'A':
        # Model A: Square beam, no 2S backdecay
        myOBE.squareBeam = True
        xMin = -myOBE.w0
        # Switch off back decay to 2S state
        myOBE.gammal = myOBE.gammal + myOBE.gammad
        myOBE.gammad = 0
        myOBE.gammas = myOBE.gammal
        myOBE.startVector = np.array([xMin, 0, 0])
        tMax = -2*xMin/myOBE.velVector[0]
    elif modelArthur == 'AD':
        # Model AD: Square beam, 2S backdecay if included in OBEs
        myOBE.squareBeam = True
        xMin = -myOBE.w0
        # Set detected signal to Lyman decays only
        myOBE.gammas = myOBE.gammal
        myOBE.startVector = np.array([xMin, 0, 0])
        tMax = -2*xMin/myOBE.velVector[0]
    elif modelArthur == 'B':
        # Model B: Gaussian beam, no 2S backdecay
        myOBE.squareBeam = False
        xMin = -3*myOBE.w0
        # Switch off back decay to 2S state
        myOBE.gammal = myOBE.gammal + myOBE.gammad
        myOBE.gammad = 0
        myOBE.gammas = myOBE.gammal
        myOBE.startVector = np.array([xMin, 0, 0])
        tMax = -2*xMin/myOBE.velVector[0]
    elif modelArthur == 'C':
        # Model C: Gaussian beam, 2S backdecay if included in OBEs
        myOBE.squareBeam = False
        xMin = -3*myOBE.w0
        # Set detected signal to Lyman decays only
        myOBE.gammas = myOBE.gammal
        myOBE.startVector = np.array([xMin, 0, 0])
        tMax = -2*xMin/myOBE.velVector[0]

    initial = np.zeros((myOBE.Y_LEN[obe_id]))
    initial[myOBE.Y_INDEX_GROUND_STATE[obe_id]] = 1
    initial_rho_norm = 1.
    input_state_sigma_x = np.nan
    input_state_x0 = np.nan

    # Set laser power
    myOBE.setLaserPower(laser_power*1e-6)
    # Set integration times to output
    tToIntegrateUnique = np.array([0, tMax])

    # Define input electric field
    Ed = lambda rd: Ed_delta(rd, delta)

    DiffE = fresnel_func.calcFresnelDisc(
        Ed,
        rIntDiff, rs, diff_fields.z_Mirror, 14, myFields.lambda0)
    t = fresnel_func.calcOverlap(rs, DiffE, np.conjugate(DiffE))

    # Calculate diffraction integrals
    k = diff_fields.k
    lambda0 = myFields.lambda0
    fit_func_id_gauss = 'GaussianBeam'
    fit_func_gauss = myFit.fitFuncs[fit_func_id_gauss]
    # Calculate diffracted field at first interaction point (forward beam)
    z = diff_fields.z_Mirror - diff_fields.dz_Interaction
    DiffEr_1 = fresnel_func.calcFresnelDisc(
        Ed, diff_fields.rdLim, rs, z, k, lambda0)
    # Fit with Gaussian
    DiffEr_1_Int = np.abs(DiffEr_1)**2
    pstart = [0, myFields.w0_Coll, np.max(DiffEr_1_Int), 0]
    fit_gaussian_1, _ = myFit.doFit(
        fit_func_gauss, rs, DiffEr_1_Int,
        np.ones(len(rs)),
        warning_as_error=False,
        pstart=pstart,
        )
    poptGr_1 = fit_gaussian_1['Popt']
    # Calculate diffracted field at second interaction point (backward beam)
    z = diff_fields.z_Mirror + diff_fields.dz_Interaction
    DiffEr_2 = fresnel_func.calcFresnelDisc(
        Ed, diff_fields.rdLim, rs, z, k, lambda0)
    # Fit with Gaussian
    DiffEr_2_Int = np.abs(DiffEr_2)**2
    pstart = [0, myFields.w0_Coll, np.max(DiffEr_2_Int), 0]
    fit_gaussian_2, _ = myFit.doFit(
        fit_func_gauss, rs, DiffEr_2_Int,
        np.ones(len(rs)),
        warning_as_error=False,
        pstart=pstart,
        )
    poptGr_2 = fit_gaussian_2['Popt']
    # Calculate overlap integral
    beam_overlap = fresnel_func.calcOverlap(
        rs, DiffEr_1, np.conjugate(DiffEr_2))
    print(
        f'Overlap integral at interaction point: {beam_overlap:.7f}'
        +f' ({100*beam_overlap/overlap0:.5f} % of maximum)'
        )
    # Intensity mismatch xi is 1 - ratio of (center) intensity of
    # backward- to forward-traveling beam
    int_mismatch = 1 - poptGr_2[2]/poptGr_1[2]

    # Interpolate electric fields from diffraction integral
    DiffEr_1_inter = fresnel_func.interEr(rs, DiffEr_1)
    DiffEr_2_inter = fresnel_func.interEr(rs, DiffEr_2)

    # Scale intensity
    intCont = fresnel_func.calcIntCont(rs, Ed(rs))
    intScale = np.sqrt(1/intCont)

    DiffEfield1 = lambda r: DiffEr_1_inter(r) * intScale
    DiffEfield2 = lambda r: np.conjugate(DiffEr_2_inter(r)) * intScale

    def Efield1(pos):

        r2 = (pos[0]**2+pos[1]**2)
        r = np.sqrt(r2)
        Efield = np.sqrt(myOBE.power_laser) * DiffEfield1(r) * np.exp(-1j*myOBE.k*pos[2])
        # E field with flat phase
#            Efield = np.sqrt(myOBE.power_laser) * np.abs(DiffEfield1(r)) * np.exp(-1j*myOBE.k*pos[2])
        # Add forward travelling reflection from collimator
#            Efield_Refl = np.exp(1j * phi) * np.sqrt(P_AR) * np.sqrt(myOBE.power_laser) * np.sqrt(2/np.pi)/w0_Refl1 * Gauss_E(r,z_Refl1,Gauss_zR(w0_Refl1,lambda0),lambda0) * np.exp(-1j*myOBE.k*pos[2])
#            Efield = Efield + Efield_Refl
#        for z_Refl, w0_Refl, phi in zip(z_Refl_List, w0_Refl_List, phis):
#            phi = phi + myOBE.domega/20e6
#            Efield_Refl = np.exp(1j * phi) * np.sqrt(P_AR) * np.sqrt(myOBE.power_laser) * np.sqrt(2/np.pi)/w0_Refl * Gauss_E(r,z_Refl,Gauss_zR(w0_Refl,lambda0),lambda0) * np.exp(-1j*myOBE.k*pos[2])
#            Efield = Efield + Efield_Refl

        return Efield

    def Efield2(pos):

        r2 = (pos[0]**2+pos[1]**2)
        r = np.sqrt(r2)
        Efield = np.sqrt(myOBE.power_laser) * DiffEfield2(r) * np.exp(1j*myOBE.k*pos[2])
        # E field with flat phase
#            Efield = np.sqrt(myOBE.power_laser) * np.abs(DiffEfield2(r)) * np.exp(1j*myOBE.k*pos[2])
        # Add backward travelling reflection from collimator
#            Efield_Refl = np.exp(1j * phi) * np.sqrt(P_AR) * np.sqrt(myOBE.power_laser) * np.sqrt(2/np.pi)/w0_Refl1 * Gauss_E(r,z_Refl1+2*dz_Interaction,Gauss_zR(w0_Refl1,lambda0),lambda0) * np.exp(1j*myOBE.k*pos[2])
#            Efield = Efield + Efield_Refl
#            for z_Refl, w0_Refl, phi in zip(z_Refl_List, w0_Refl_List, phis):
#                Efield_Refl = np.exp(1j * phi) * np.sqrt(P_AR) * np.sqrt(myOBE.power_laser) * np.sqrt(2/np.pi)/w0_Refl * Gauss_E(r,z_Refl+2*dz_Interaction,Gauss_zR(w0_Refl,lambda0),lambda0) * np.exp(1j*myOBE.k*pos[2])
#                Efield = Efield + Efield_Refl

        return Efield

    # Set electric fields
    myOBE.Efield1 = Efield1
    myOBE.Efield2 = Efield2

    obe_func = lambda t, y: myOBE.derivs_handles[obe_id](y, t)

    oderesults = np.zeros((len(detunings), len(tToIntegrateUnique), len(initial)))
    num_reps = 1
    run_times = np.zeros(num_reps)
    for i_rep in range(num_reps):
        start_time = time.time()
        for i_detuning, detuning in enumerate(detunings):
            print(
                f'{detuning:.0f} Hz')
            myOBE.domega = 2*np.pi*detuning
            sol = scipy.integrate.solve_ivp(
                obe_func, (np.min(tToIntegrateUnique), np.max(tToIntegrateUnique)),
                initial, t_eval=tToIntegrateUnique, method=ode_solver_method,
                atol=1e-10, rtol=5e-11, first_step=1e-12)
            oderesults[i_detuning] = sol.y.transpose()
        print('Calculation took {:.2f} s'.format(time.time()-start_time))
        run_times[i_rep] = time.time()-start_time
    print(
        'Calculation took {:.2f} s on average for {:d} trials'
        .format(np.mean(run_times), num_reps))
    scan_results[i] = oderesults

    # Fit line
    # Use Lyman decays as signal
    signals = oderesults[:, -1, myOBE.Y_INDEX_SIGNAL_LYMAN[obe_id]]
    x = detunings
    y = signals
    ysigma = np.ones(len(y))
    if fit_line:
        line_fits = myFit.fitLine(x, y, ysigma)
        fit_result = line_fits[fit_func_id]
        fit_func = myFit.fit_funcs[fit_func_id]
        sr_fit = myFit.fit_result_to_series(fit_result, fit_func)
        print('Fit result: CFR: {:.4f} Hz'.format(sr_fit['CFR_Value']))

    dfApp = {
        'OBECalcUID': obe_calc_uid,
        'SessionName': session_name,
        'FieldID': field_id,
        'BeamOverlap': beam_overlap,
        'IntMismatch': int_mismatch,
        'Delta': delta,
        'Delta0': delta0,
        'Lambda': lambda0,
        'Coll_f': myFields.f_Coll,
        'Coll_w0': myFields.w0_Coll,
        'Fiber_w0': myFields.w0_Fiber,
        'Coll_zR': myFields.zR_Coll,
        'Fiber_zR': myFields.zR_Fiber,
        **{
            key: elem for key, elem in field.items()
            if key not in ['EDelta', 'E', 'EXY', 'DeltaList', 'Delta']
            },
        **myFit.fit_result_to_dict(fit_gaussian_1, fit_func_gauss, 'E1_GaussFit'),
        **myFit.fit_result_to_dict(fit_gaussian_2, fit_func_gauss, 'E2_GaussFit'),
        }
    dfApp['UpperLevelN'] = upper_level_n
    dfApp['LineSimPower'] = laser_power
    dfApp['2SnPOBE'] = obe_id
    dfApp['ModelArthur'] = modelArthur
    dfApp['Vz'] = vz
    dfApp['TransverseVelocity'] = transverse_velocity
    dfApp['FitFuncID'] = fit_func_id
    dfApp['FitSignal'] = 'Lyman'
    dfApp['MaxSignal'] = np.max(y)
    dfApp['V'] = v
    dfApp['AlphaOffset'] = alpha_offset
    dfApp['NDeltapDecay'] = myOBE.NUM_DELTA_P_DECAY[obe_id]
    if myOBE.NUM_DELTA_P_DECAY[obe_id] > 0:
        dfApp['DeltapDecay1'] = dpd1
        dfApp['DeltapDecay{:d}'.format(myOBE.NUM_DELTA_P_DECAY[obe_id]+1)] = -dpd1
    else:
        dfApp['DeltapDecay1'] = np.nan
        dfApp['DeltapDecay2'] = np.nan
    dfApp['NDetunings'] = len(detunings)
    dfApp['MeanDetuning'] = np.mean(detunings)
    dfApp['SignalsSaved'] = (
        'Lyman' if myOBE.Y_INDEX_SIGNAL_BALMER[obe_id] == -1 else 'LymanBalmer')
    dfApp['SignalColumnLyman'] = myOBE.Y_INDEX_SIGNAL_LYMAN[obe_id]
    dfApp['SignalColumnBalmer'] = myOBE.Y_INDEX_SIGNAL_BALMER[obe_id]
    dfApp['ODESolverMethod'] = ode_solver_method
    dfApp['InputState'] = input_state
    dfApp['InitialRhoNorm'] = initial_rho_norm
    dfApp['InputStateSigmaPos'] = input_state_sigma_x
    dfApp['InputStateX0'] = input_state_x0
    dfApp['IntegrationStartPos'] = xMin
    if fit_line:
        dfApp = {**dfApp, **sr_fit}

    resultsOBE = resultsOBE.append(dfApp, ignore_index = True)

scan_detunings = np.array(scan_detunings)
scan_results = np.array(scan_results)

#%% Save results

output_dir = 'obe_results'
filename_prefix = time.strftime('%Y-%m-%d %H-%M-%S')
output_filename = (
    filename_prefix
    + f' {obe_calc_uid} {session_name}')
print(f'Wrote files \'{output_filename} ...\'')
# Save dataframe
resultsOBE.to_csv(os.path.join(
    output_dir, output_filename+' resultsOBE.dat'))
# Save OBE results
np.save(os.path.join(
    output_dir, output_filename+' detunings.npy'),
    scan_detunings)
np.save(os.path.join(
    output_dir, output_filename+' scanresults.npy'),
    scan_results)
