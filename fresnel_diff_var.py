# -*- coding: utf-8 -*-
"""
Created on Thu May  4 11:51:15 2017

@author: Lothar Maisenbacher/MPQ

Various diffraction calculations
"""

import numpy as np
import scipy.integrate
import scipy.special
import scipy.optimize
import scipy.interpolate
import matplotlib.pyplot as plt
import matplotlib.gridspec
import time
import os
import LightPipes
import datetime

# pyhs
import pyhs.fit

import fresnel_func
import diff_fields

# Init fit class
myFit = pyhs.fit.Fit()

import pyha.plts as plts
plts.set_params()

#%% Global constants and settings

myFields = diff_fields.myFields

# Wavelength
#lambda0 = 380e-9
lambda0 = 410e-9
#lambda0 = 486e-9

# Fiber beam waist (MFD/2)
#w0_Fiber = 2.5e-6/2  # 380 nm
#w0_Fiber = 3.6e-6/2  # 410 nm
w0_Fiber = 4.6e-6/2  # 486 nm

# Beam and collimator properties
f_Coll = 30e-3  # Focal length of collimator

myFields.lambda0 = lambda0
myFields.w0_Fiber = w0_Fiber
myFields.update_optical_params()
zR_Fiber = myFields.zR_Fiber
zr_coll = myFields.zR_Coll
w0_Coll = myFields.w0_Coll

# Geometry
z_Mirror = 0.3  # Distance of HR mirror from collimator
dz_Interaction = 0.15  # Distance of interaction region from mirror, i.e. first interaction point at dz_Interaction after collimator, second interaction point at z_Mirror + dz_Interaction after collimator

# Integration settings
k = 14  # 2**k+1 integration samples
rdLim = 4*w0_Coll  # Upper integration limit for diffraction integral

# Upper limit for integration due to optics size
rMaxInt = 12.5e-3  # Default: 1"

# Number of fiber-collimator distances delta to evaluate to find optimal delta
num_deltas = 20
delta_list_0 = None

myFields.fields['Gaussian_s=0.3'] = {
    'Source': 'Analytic',
    'SpherAberS': 0.3,
    'Delta': 2.13686974e-06,
    'DeltaList': myFields.fields['Gaussian_s=-0.3']['DeltaList'],
    }

myFields.process_fields()
fields = myFields.fields

#%% Select electric field

# Analytically defined Gaussian beam
# field_id = 'Gaussian_s=0'
# Analytically defined Gaussian beam with spherical aberration coefficient S=-0.3
# field_id = 'Gaussian_s=-0.3'
# field_id = 'Gaussian_s=0.3'
# B. Halle 4-lens collimator, misaligned
#field_id = 'BHaV11_Mis_L3_1deg'
# B. Halle 4-lens collimator
# field_id = 'BHaV11_410nm_MFD3.6um_2048auto'
# B. Halle 3-lens collimator
field_id = 'BHa_410nm_MFD3.6um_2048auto'

field = fields[field_id]
Ed = field['E']
Ed_xy = field['EXY']
Ed_delta = field['EDelta']
delta_list_0 = field.get('DeltaList')

#%% Plot initial electric field

x = np.linspace(0, 3*w0_Coll, 10000)
Ed_int = np.abs(Ed(x))**2

# Fit intensity with Gaussian
fit_func_id = 'GaussianBeam'
fit_func = myFit.fitFuncs[fit_func_id]
pstart = [0, 2e-3, 1, 0]
fit_gaussian, _ = myFit.do_fit(
    fit_func, x, Ed_int,
    np.ones(len(x)),
    warning_as_error=False,
    pstart=pstart,
    )
poptG = fit_gaussian['Popt']

## Plot
# Prepare figure
fig = plt.figure(
    'Initial electric field',
    figsize=(5, 5), constrained_layout=True)
plt.clf()
gs = matplotlib.gridspec.GridSpec(
    2, 1,
    width_ratios=[1],
    height_ratios=[1,1],
    figure=fig,
    )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1], sharex=ax1)

# Plot intensity and phase
ax1.plot(
    x, Ed_int,
    **plts.get_point_params(),
    label='$w_0$ = {:.2f} mm'.format(poptG[1]*1e3))
ax2.plot(
    x, np.unwrap(np.angle(Ed(x))),
    **plts.get_point_params())

ax1.legend(
    **plts.get_leg_params())

ax1.set_ylabel('Intensity (arb. u.)')
ax2.set_ylabel('Phase (rad)')
ax2.set_xlabel('Transverse position (m)')

#%% Plot ZEMAX physical optics propagation (POP) result if available

if field['Source'] == 'POP':

    # Read files and process data
    POP_amp, POP_phase = fresnel_func.readZemaxPOP(field['Filepath'])
    mask = np.abs(POP_amp[:,0]) < rMaxInt

    # Interpolate
    POP_amp_inter, POP_phase_inter = (
        fresnel_func.interpolatePOP(POP_amp[mask], POP_phase[mask]))

    # Fit intensity with Gaussian
    fit_func_id = 'GaussianBeam'
    fit_func = myFit.fitFuncs[fit_func_id]
    pstart = [0, 2e-3, 1, 0]
    POP_int = POP_amp.copy()
    POP_int[:,1] = POP_int[:,1]**2
    fit_gaussian, _ = myFit.doFit(
        fit_func, POP_int[:,0], POP_int[:,1],
        np.ones(len(POP_int[:,0])),
        warning_as_error=False,
        pstart=pstart,
        )
    poptG = fit_gaussian['Popt']

    #print(poptG[1])
    #print(Gauss_zR(poptG[1],lambda0))
    #zR = Gauss_zR(poptG[1],lambda0)

    # Built array of electric field as radial function, i.e. assumes spherical symmetry and throws away data for negative r
    #mask = POP_amp[:,0] >= 0
    #POP_rd = POP_amp[mask,0]
    #POP_Ed = POP_amp[mask,1] * np.exp(1j*POP_phase[mask,1])

    ## Plot
    # Prepare figure
    fig = plt.figure(
        'Zemax POP',
        figsize=(5, 5), constrained_layout=True)
    plt.clf()
    gs = matplotlib.gridspec.GridSpec(
        2, 1,
        width_ratios=[1],
        height_ratios=[1,1],
        figure=fig,
        )
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1], sharex=ax1)

    # Plot intensity and phase
    x = np.linspace(-rMaxInt,rMaxInt,10000)
    ax1.plot(
        POP_amp[:,0], POP_amp[:,1],
        **plts.get_point_params(),
        label='$w_0$ = {:.2f} mm'.format(poptG[1]*1e3))
    #ax1.plot(x,np.sqrt(fitfunc.GaussFit(x,*poptG)), linestyle='-', color='r', linewidth=3)
    ax2.plot(
        POP_phase[:,0], POP_phase[:,1],
        **plts.get_point_params())

    ax1.plot(
        x, POP_amp_inter(x), linestyle='-')
    ax2.plot(
        x, POP_phase_inter(x), linestyle='-')

    #Ed = lambda r: POP_amp_inter(r) * np.exp(1j*POP_phase_inter(r))
    #ax1.plot(x, np.abs(Ed(x)), linestyle='-')
    #ax2.plot(x, np.unwrap(np.angle(Ed(x))), linestyle='-')

    ax1.legend(
        **plts.get_leg_params())

    ax1.set_ylabel('Intensity (arb. u.)')
    ax2.set_ylabel('Phase (rad)')
    ax2.set_xlabel('Transverse position (m)')

    ax1.set_xlim([min(x),max(x)])
    ax1.set_title('Zemax POP: ' + field_id)

    #plt.savefig(folder + 'Plots\\' + filename + '.pdf')

#%% Calculate beam profile at interaction points and at mirror positions

# Sampling of diffracted pattern
Npoints = 500  # Number of points to calculate
r = np.linspace(0, fresnel_func.rInt(3*w0_Coll, rMaxInt), Npoints)
x = np.hstack((-np.flipud(r[1:]),r))
Ed_x = np.hstack((np.flipud(Ed(r[1:])), Ed(r)))

calc_fresnel_discrete = fresnel_func.calcFresnelDisc

# Diffraction calculations
start_time = time.time()
# Calculate diffracted field at mirror position
z = z_Mirror
DiffEr = calc_fresnel_discrete(
    Ed, fresnel_func.rInt(rdLim, rMaxInt), r, z, k, lambda0)
DiffEr_3 = DiffEr
DiffE = np.hstack((np.flipud(DiffEr[1:]), DiffEr))
DiffE_3 = DiffE

print(
    'Overlap integral between forward (1) and backward (2) beams at mirror position: {}'
    .format(fresnel_func.calcOverlap(r, DiffEr, np.conjugate(DiffEr))))

# Calculate diffracted field at first interaction point (forward beam)
z = z_Mirror - dz_Interaction
DiffEr = calc_fresnel_discrete(
    Ed, fresnel_func.rInt(rdLim,rMaxInt), r, z, k, lambda0)
DiffEr_1 = DiffEr
DiffE = np.hstack((np.flipud(DiffEr[1:]), DiffEr))
DiffE_1 = DiffE

# Calculate diffracted field at second interaction point (backward beam)
z = z_Mirror + dz_Interaction
DiffEr = calc_fresnel_discrete(
    Ed, fresnel_func.rInt(rdLim,rMaxInt), r, z, k, lambda0)
DiffEr_2 = DiffEr
DiffE = np.hstack((np.flipud(DiffEr[1:]), DiffEr))
DiffE_2 = DiffE

print(
    '1 - overlap integral between forward (1) and backward (2) beams at mirror position: {:.3e}'
    .format(1-fresnel_func.calcOverlap(r, DiffEr_1, np.conjugate(DiffEr_2))))
print(
    'Peak intensity mismatch without fit (peak intensity beam 1 / peak intensity beam 2 - 1): {:.3e}'
    .format(np.abs(DiffEr_1[0])**2/np.abs(DiffEr_2[0])**2-1))

print('Computation time for diffraction calculation: {:.2f} s'
    .format(time.time()-start_time))

#%% Repeat calculation using LightPipes module and FFT implementation of Fresnel integral

# Grid for numerical calculation
grid_size = 2 * 4 * w0_Coll
grid_dimension = 2000 # Should be even for LightPipes.Forvard method to give valid results
ix_center = int(grid_dimension/2)

# Set LightPipes parameters
x_lp = np.linspace(-grid_size/2, grid_size/2, grid_dimension)
r_lp = x_lp[ix_center:]
mask_lp = (x_lp >= np.min(x)) & (x_lp <= np.max(x))
#lp_prop_func = LightPipes.Fresnel
lp_prop_func = LightPipes.Forvard

field_0 = LightPipes.Begin(grid_size, lambda0, grid_dimension)

# Propagation direction has different sign in LightPipes as in fresnel_func,
# flip sign of z direction for both propagation and curvature of field.
# See Eq. (4) of `https://opticspy.github.io/lightpipes/manual.html#direct-integration-as-a-convolution-fft-approach`
def flip_phase(E):
    return np.abs(E)*np.exp(-1j*np.angle(E))
X, Y = np.meshgrid(x_lp, x_lp)
field_coll = LightPipes.Begin(grid_size, lambda0, grid_dimension)
field_coll.field = Ed_xy(X, Y)
field_0 = field_coll
Ed_x_lp = flip_phase(field_0.field[ix_center])

# Diffraction calculations
start_time = time.time()
# Calculate diffracted field at mirror position
z = z_Mirror
field_3 = lp_prop_func(-z, field_coll)
DiffE_3_lp = flip_phase(field_3.field[ix_center])
DiffEr_3_lp = DiffE_3_lp[ix_center:]

overlap = fresnel_func.calcOverlap(r_lp, DiffEr_3_lp, np.conjugate(DiffEr_3_lp))
print(
    'Overlap integral between forward (1) and backward (2) beams at mirror position: '
    +'\n'+f'{overlap:.7f} (1-...: {1-overlap:.3e})'
    )

# Calculate diffracted field at first interaction point (forward beam)
z = (z_Mirror - dz_Interaction)
field_1 = lp_prop_func(-z, field_coll)
DiffE_1_lp = flip_phase(field_1.field[ix_center])
DiffEr_1_lp = DiffE_1_lp[ix_center:]

# Calculate diffracted field at second interaction point (backward beam)
z = (z_Mirror + dz_Interaction)
field_2 = lp_prop_func(-z, field_coll)
DiffE_2_lp = flip_phase(field_2.field[ix_center])
DiffEr_2_lp = DiffE_2_lp[ix_center:]

print(
    f'Computation time for diffraction calculation: {time.time()-start_time:.2f} s')

#%% Plot beam profile at interaction points and at mirror positions

# Prepare fit
fit_func_id = 'GaussianBeam'
fit_func = myFit.fitFuncs[fit_func_id]

# Prepare figures
fig = plt.figure(
    'Beam profile at interaction points and mirror positions',
    figsize=(7, 6), constrained_layout=True)
plt.clf()
gs = matplotlib.gridspec.GridSpec(
    2, 2,
    width_ratios=[1, 1],
    height_ratios=[1,1],
    figure=fig,
    )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[1])
ax4 = plt.subplot(gs[3])

e_phases = {}
gauss_fits = {}
for _, (key, type_, E, label) in enumerate(zip(
#    ['Coll', 'Int1', 'Mirr', 'Int2'],
#    ['1D'] * 4,
#    [Ed_x, DiffE_1, DiffE_3, DiffE_2],
#    ['After collimator', '1st interaction point', 'At mirror', '2nd interaction point']
    ['Coll', 'CollLP', 'Int1', 'Int1LP', 'Mirr', 'MirrLP', 'Int2', 'Int2LP'],
#    [x, x_lp[mask_lp]] * 4,
    ['1D', '2DLP'] * 4,
    [Ed_x, Ed_x_lp[mask_lp], DiffE_1, DiffE_1_lp[mask_lp],
        DiffE_3, DiffE_3_lp[mask_lp], DiffE_2, DiffE_2_lp[mask_lp]],
    ['Collimator', 'Collimator LP',
     '1st interaction point', '1st interaction point LP',
     'At mirror', 'At mirror LP',
     '2nd interaction point', '2nd interaction pointLP']
    )):

    DiffE_Int = np.abs(E)**2
    DiffE_Phase = np.unwrap(np.angle(E))

    if type_ == '1D':
        x_ = x
    elif type_ == '2DLP':
        x_ = x_lp[mask_lp]
        DiffE_Phase *= -1
    # Fit with Gaussian
    fit_gaussian, _ = myFit.do_fit(
        fit_func, x_, DiffE_Int, np.ones(len(x_)),
        warning_as_error=False,
        pstart=[0, w0_Coll, np.max(DiffE_Int), 0],
        )
    sr_fit = myFit.fit_result_to_series(fit_gaussian, fit_func)
    gauss_fits[key] = sr_fit
    hInt0 = ax1.plot(
        x_, DiffE_Int,
        label=label+'\n' + r'$w_0$ = {:.2f} mm, $I_\mathrm{{max}}$ = {:.3e}'
            .format(fit_gaussian['Popt'][1]*1e3, fit_gaussian['Popt'][2]))
    ax1.plot(
        x_, fit_func['Func'](x_, *fit_gaussian['Popt']),
        linestyle=':', color=hInt0[0].get_color())

    # Plot phase of beam
    ixmin = np.argmin(np.abs(x_))
#    print(DiffE_Phase[ixmin])
#    print(x_[ixmin])
    DiffE_Phase = DiffE_Phase-DiffE_Phase[ixmin]
    e_phases[key] = DiffE_Phase
    ax2.plot(x_, DiffE_Phase, label=label)

# Plot difference in phase at the interaction points,
# taking into account that phase is flipped at mirror
y = e_phases['Int1'] + e_phases['Int2']
ax4.plot(x, y)
y = e_phases['Int1LP'] + e_phases['Int2LP']
ax4.plot(x_lp[mask_lp], y)

# Plot difference in intensities at the two interaction points
y = (np.abs(DiffE_1)**2-np.abs(DiffE_2)**2)/((np.abs(DiffE_1)**2+np.abs(DiffE_2)**2)/2)
ax3.plot(x, y)
y = (np.abs(DiffE_1_lp)**2-np.abs(DiffE_2_lp)**2)/((np.abs(DiffE_1_lp)**2+np.abs(DiffE_2_lp)**2)/2)
ax3.plot(x_lp[mask_lp], y[mask_lp])

print('Peak intensity mismatch from fit (peak intensity beam 1 / peak intensity beam 2 - 1): {:.3e}'
    .format(gauss_fits['Int1']['Amp_Value']/gauss_fits['Int2']['Amp_Value']-1))
print('Peak intensity mismatch from fit (peak intensity beam 1 / peak intensity beam 2 - 1): {:.3e}'
    .format(gauss_fits['Int1LP']['Amp_Value']/gauss_fits['Int2LP']['Amp_Value']-1))

#for ax in [ax1, ax2, ax3, ax4]:
#    ax.set_xlim([np.min(x), np.max(x)])

# Add legends
ax1.legend(
    **plts.get_leg_params())
ax2.legend(
    **plts.get_leg_params())
ax2.set_xlabel('Transverse position (m)')
ax4.set_xlabel('Transverse position (m)')
ax1.set_ylabel('Intensity\n(relative to peak intensity in fiber)')
ax2.set_ylabel('Phase (rad)')
ax3.set_ylabel('Rel. deviation in intensity\nbetween interaction points')
ax4.set_ylabel('Difference in phase\nbetween interaction points (rad)')

filenamePrefix = '{}_'.format(time.strftime('%Y-%m-%d'))
#plt.savefig('Fresnel_plots\\Diff_' + filenamePrefix + sessionName + '.pdf')

#%% Interpolate electric fields

# Interpolate field for first beam at interaction point, using linear interpolation
DiffEr_1_inter = fresnel_func.interEr(r, DiffEr_1)

# Interpolate field for second beam at interaction point, using linear interpolation
DiffEr_2_inter = fresnel_func.interEr(r, DiffEr_2)

# Plot results of interpolation
#ax1.plot(r,np.abs(DiffEr_1_inter(r))**2)
#ax2.plot(r,np.unwrap(np.angle(DiffEr_1_inter(r))) - np.unwrap(np.angle(DiffEr_1_inter(r)))[0])

#ax1.plot(r,np.abs(DiffEr_2_inter(r))**2)
#ax2.plot(r,np.unwrap(np.angle(DiffEr_2_inter(r))) - np.unwrap(np.angle(DiffEr_2_inter(r)))[0])

#ax3.plot(r,np.abs(DiffEr_1_inter(r))**2/np.max(np.abs(DiffEr_1_inter(r))**2)-np.abs(DiffEr_2_inter(r))**2/np.max(np.abs(DiffEr_2_inter(r))**2))
#ax4.plot(r,np.unwrap(np.angle(DiffEr_1_inter(r))) - np.unwrap(np.angle(DiffEr_1_inter(r)))[0]+(np.unwrap(np.angle(DiffEr_2_inter(r))) - np.unwrap(np.angle(DiffEr_2_inter(r)))[0]))

# Electric fields that drive the transition in OBE simulation
# Scale amplitude such that it corresponds to a total power of 1 W
intScale = np.sqrt(2/np.pi)/w0_Fiber
DiffEfield1 = lambda r: DiffEr_1_inter(r) * intScale
DiffEfield2 = lambda r: np.conjugate(DiffEr_2_inter(r)) * intScale

#%% Backcoupling as function of collimator displacement delta,
# evaluated by propagating with both a Fresnel integral (evaluated at discrete points)
# and a FFT algorithm (LightPipes.Forvard).

r = np.linspace(0, 3*fresnel_func.Gauss_w0(zr_coll, lambda0), 20)
# Array of sampling points for delta
delta_list = field['DeltaList']
#delta_list = np.linspace(-13e-6, -7e-6, 20)

# LightPipes
grid_size = 2 * 4 * w0_Coll
grid_dimension = 200
ix_center = int(grid_dimension/2)
x_lp = np.linspace(-grid_size/2, grid_size/2, grid_dimension)
r_lp = x_lp[ix_center:]
diff_e = LightPipes.Begin(grid_size, lambda0, grid_dimension)
X, Y = np.meshgrid(x_lp, x_lp)

backcoupling = np.zeros(len(delta_list))
backcoupling_lp = np.zeros(len(delta_list))
for i, delta in enumerate(delta_list):
#    print(delta)
    DiffE = fresnel_func.calcFresnelDisc(
        lambda rd: Ed_delta(rd, delta),
        4 * fresnel_func.Gauss_w0(zr_coll, lambda0),
        r, z_Mirror, 14, lambda0)
    backcoupling[i] = fresnel_func.calcOverlap(
        r, DiffE, np.conjugate(DiffE))

    t = lambda x, y: Ed_delta(np.sqrt(x**2+y**2), delta)
    diff_e.field = t(X, Y)
    diff_e = LightPipes.Forvard(
        -z_Mirror, diff_e
        )
    diff_e_r = diff_e.field[ix_center, ix_center:]
    backcoupling_lp[i] = fresnel_func.calcOverlap(
        r_lp, diff_e_r, np.conjugate(diff_e_r))

#    print(backcoupling[i])
#    print(backcoupling_lp[i])

fig = plt.figure(
    'Overlap integral vs delta', figsize=(5, 5), constrained_layout=True)
plt.clf()
ax1 = plt.gca()
h_bc = ax1.plot(
    delta_list, backcoupling,
    **plts.get_point_params(**{
        'linestyle': '-',
        'linewidth': '1',
        'label': 'Fresnel (discrete sampling)',
        }))
h_bc_lp = ax1.plot(
    delta_list, backcoupling_lp,
    **plts.get_point_params(**{
        'linestyle': '--',
        'linewidth': '1',
        'label': 'LightPipes.Forvard (FFT)',
        }))

# Fit with quadratic function
fitQu = lambda x,a,x0,y0: a*(x-x0)**2 + y0
pstart = [-1e9, 0, 0.8]
popt, pcov = scipy.optimize.curve_fit(
    fitQu, delta_list, backcoupling, pstart)
popt_lp, pcov_lp = scipy.optimize.curve_fit(
    fitQu, delta_list, backcoupling_lp, pstart)
xFit = np.linspace(min(delta_list), max(delta_list), 1000)
ax1.plot(
    xFit, fitQu(xFit, *popt))
ax1.legend(
    **plts.get_leg_params())
ax1.set_xlabel(r'Distance fiber-collimator $\delta$ (m)')
ax1.set_ylabel('Overlap integral')
print(popt)
print(popt_lp)

#%% Find optimal backcoupling with least square optimization and additional
# parabola fit around the optimum to determine behavior around optimum

r = np.linspace(
    0, fresnel_func.rInt(3 * fresnel_func.Gauss_w0(zr_coll, lambda0), rMaxInt), 20)
rIntDiff = fresnel_func.rInt(4*fresnel_func.Gauss_w0(zr_coll, lambda0), rMaxInt)
def overlapDelta(deltas):
    deltas = np.array([deltas]).flatten()
    DiffEs = np.array([
        fresnel_func.calcFresnelDisc(
            lambda rd: Ed_delta(rd, delta), rIntDiff, r, z_Mirror, 14, lambda0)
        for delta in deltas])
    return np.array([
        fresnel_func.calcOverlap(r, DiffE, np.conjugate(DiffE))
        for DiffE in DiffEs])

fitTol = 1e-3
startTime = time.time()
poptLS, cov_x, infodict, mesg, ier = scipy.optimize.leastsq(
    lambda delta: overlapDelta(delta)-1, 0, full_output=True, xtol=fitTol)
delta0 = poptLS[0]
print(infodict['nfev'])
print(time.time()-startTime)
print(delta0)
overlap0 = overlapDelta(delta0)

# Evaluate points around found optimum and fit with quadratic function
deltaOptStart = [-1e9, delta0, 0.9]
delta_opt_range = 3e-6

delta_list = np.linspace(-delta_opt_range+delta0, delta_opt_range+delta0, 5)
backcoupling = overlapDelta(delta_list)

fig = plt.figure(
    'Overlap integral vs delta', figsize=(5, 5), constrained_layout=True)
plt.clf()
ax1 = plt.gca()
ax1.cla()
ax1.plot(delta_list, backcoupling)

# Fit with quadratic function
fitQu = lambda x,a,x0,y0: a*(x-x0)**2 + y0
pstart = deltaOptStart
popt, pcov = scipy.optimize.curve_fit(
    fitQu, delta_list, backcoupling, pstart)
xFit = np.linspace(min(delta_list), max(delta_list),1000)
ax1.plot(xFit, fitQu(xFit, *popt))
print(popt)

ax1.set_xlabel(r'Distance fiber-collimator $\delta$ (m)')
ax1.set_ylabel('Overlap integral')

dy = 1e-2
delta1 = -np.sqrt((1-dy - popt[2])/popt[0]) + popt[1]
delta2 = np.sqrt((1-dy - popt[2])/popt[0]) + popt[1]

#%% Plot beam profile after additional lens as function of Gouy phase
# This corresponds to the M^2 measurement

zd = 0  # Position where additional lens is placed (in m)

f = 1  # Focal length of additional lens (in m)

# Rayleigh length of beam after additional lens
zr_lens = fresnel_func.lens_zR(zr_coll, f)

# Number of plots/positions evaluated
N = 21

# Directly define z-positions after lens at which beam profile is evaluated
# z = np.linspace(0.5, 2., N)

# Define gouys phase of evaluated positions (in fraction of pi/2)
gouys = np.linspace(-0.95, 0.95, N)
# gouys = np.linspace(-0.05, 0.05, N)
# gouys = np.linspace(0.6, 0.95, N)
# z-positions after lens correspond to gouys phases
# z = np.tan(gouys*np.pi+np.arctan(-zr_coll/f))*zr_lens + f
z = np.tan(gouys*np.pi/2)*zr_lens + f

# Define Rayleigh lengths relative to focus at which beam profile is evaluated
zrs = np.array([0, 1, 1.5, 2, 2.5, 3, 3.5, 5, 10])
zrs = np.unique(np.hstack((-np.flipud(zrs), zrs)))
# zrs = np.array([2.5])
z = zrs*zr_lens+f

N_ = len(z)
fig = plt.figure(
    'M2 plot', figsize=(6, 7), constrained_layout=True)
plt.clf()
Nh = 4
Nv = int(np.ceil(N_/Nh))
gs = matplotlib.gridspec.GridSpec(
    Nv, Nh,
    width_ratios=np.ones(Nh),
    height_ratios=np.ones(Nv),
    figure=fig,
    )

# Apply M2 lens to electric field
Ed_m2 = lambda rd: (
    Ed(rd) * fresnel_func.lens_Phase(rd, f, lambda0))

gaussian_profile_int_1d = lambda x, w: np.exp(-2*x**2/w**2)

for i, za in enumerate(z):

    ax = fig.add_subplot(gs[i])

    w = fresnel_func.Gauss_w(za-f, zr_lens, lambda0)
    # w = 1e-3
    print(f'z = {za:.3e} m')
    print(f'Guide beam: w = {1e3*w:.2f} mm')
    r = np.linspace(0, 3*w, 100)
    x = np.hstack((-np.flipud(r[1:]), r))
#    r = np.linspace(-0.3*Gauss_w0(zR),0.3*Gauss_w0(zR),100)
    DiffEr = fresnel_func.calcFresnelDisc(
        Ed_m2, 4*fresnel_func.Gauss_w0(zr_coll, lambda0), r, za, 10, lambda0)
    DiffE = np.hstack((np.flipud(DiffEr[1:]), DiffEr))

#    x = np.linspace(-3,3,100)
#    y = np.unwrap(np.angle(DiffE))
    # Intensity
    y = np.abs(DiffE)**2
    # Normalized intensity
    y = y/np.max(y)
    # Fit 1D Gaussian
    pstart = [w]
    popt, pcov = scipy.optimize.curve_fit(
        gaussian_profile_int_1d, x, y, pstart)
    print(f'Fit: w = {1e3*popt[0]:.2f} mm')

    x_plot = x*1e3
    # Show calculated intensity
    ax.plot(x_plot, y)
    # Show 1D Gaussian
    ax.plot(x_plot, gaussian_profile_int_1d(x, *popt))
    ax.set_xticks([min(x_plot), np.mean(x_plot), max(x_plot)])
    gouy = (np.arctan((za-f)/zr_lens))/(np.pi)
    ax.set_title(
        rf'$\psi(z)$ = {gouy:.3f} $\pi$'
        +'\n'+rf'($z_\mathrm{{R}}$ = {(za-f)/zr_lens:.1f}, $z$ = {za:.3f} m)',
        **plts.get_title_params())

    if i == 0:
        ax.set_ylabel(' ')

ax.set_xlabel(' ')
fig.text(0.5, 0.01, 'Transverse position (mm)', ha='center')
fig.text(0.01, 0.5, 'Intensity (arb. u.)', va='center', rotation='vertical')
fig.suptitle(
    f'Diffraction pattern after $f$ = {f:.2f} m lens as function of Gouy phase for {field_id}',
    **plts.get_title_params())

time_prefix = datetime.datetime.utcnow().strftime('%Y-%m-%d-%H-%M-%S')
# fig.savefig(os.path.join(
#     'fresnel_plots', f'{time_prefix}_M2_{field_id}.pdf'))

#%% Plot field after M2 lens

x = np.linspace(0, 3*w0_Coll, 10000)
Ed_m2_int = np.abs(Ed_m2(x))**2

# Fit intensity with Gaussian
fit_func_id = 'GaussianBeam'
fit_func = myFit.fitFuncs[fit_func_id]
pstart = [0, 2e-3, 1, 0]
fit_gaussian, _ = myFit.do_fit(
    fit_func, x, Ed_m2_int,
    np.ones(len(x)),
    warning_as_error=False,
    pstart=pstart,
    )
poptG = fit_gaussian['Popt']

## Plot
# Prepare figure
fig = plt.figure(
    'Electric field after M2 lens',
    figsize=(5, 5), constrained_layout=True)
plt.clf()
gs = matplotlib.gridspec.GridSpec(
    2, 1,
    width_ratios=[1],
    height_ratios=[1,1],
    figure=fig,
    )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1], sharex=ax1)

# Plot intensity and phase
ax1.plot(
    x, Ed_m2_int,
    **plts.get_point_params(),
    label='$w_0$ = {:.2f} mm'.format(poptG[1]*1e3))
ax2.plot(
    x, np.unwrap(np.angle(Ed_m2(x))),
    **plts.get_point_params())

ax1.legend(
    **plts.get_leg_params())

ax1.set_ylabel('Intensity (arb. u.)')
ax2.set_ylabel('Phase (rad)')
ax2.set_xlabel('Transverse position (m)')

#%% Calculate overlap integral of far field fiber modes

## Sampling of diffracted pattern
#Npoints = 500 # Number of points to calculate
#r = np.linspace(0,rInt(4e-3,rMaxInt),Npoints)
#x = np.hstack((-np.flipud(r[1:]),r))
#ixmin = np.argmin(np.abs(x))
#
#def EFieldFiberMode(r,a,kT,gamma,s):
#    r = np.array([r]).flatten()
#    result = np.empty(len(r))
#    result[r<=a] = scipy.special.j0(kT*r[r<=a])
#    result[r>a] = s*scipy.special.k0(gamma*r[r>a])
#    return result
#
##410nm:
#a=1.5e-6
#kT=1.07920201798e6
#gamma=1.08803993162e6
#s=2.46475741617
#
#Ed = lambda r: EFieldFiberMode(r,a,kT,gamma,s)
#
## Diffraction calculations
## Calculate diffracted field at mirror position
#z = 4e-3
#k = 16
#DiffEr = calcFresnelDisc(Ed,20e-6,r,z,k,lambda0)
##DiffEr_3 = DiffEr
##DiffE = np.hstack((np.flipud(DiffEr[1:]),DiffEr))
##DiffE_3 = DiffE
#
#ax1 = plt.gca()
##scale = 6e-3
##ax1.semilogy(r,np.abs(Ed(r*scale))**2/np.max(np.abs(Ed(r*scale))**2))
##ax1.semilogy(r,np.abs(DiffEr)**2/np.max(np.abs(DiffEr)**2))
#ax1.plot(r,np.abs(DiffEr)**2/np.max(np.abs(DiffEr)**2))
#
##DiffEr_inter = interEr(r, DiffEr)

#%%

#lambda0 = 410e-9
#k0 = 2*np.pi/lambda0
#w0_Fiber=1.703e-6
#zR_Fiber = Gauss_zR(w0_Fiber,lambda0)
#lagcoeff=[2.1362844610291947,0.017182575299230802,0.10749844665878426,-0.07069515744311301,-0.019833354214996164,-0.039726501848900926,-0.012171424556575591,-0.011242833018581993,0.0015588379818807356,0.002713149312489355,0.007211929945945706,0.006446388334504639,0.006933819288008384,0.005167670043219531,0.00417056688775183,0.002359510288427045,0.0011794426439560239,-0.0001673536882193498,-0.0009888505227512228,-0.0017332598150315473,-0.0020768853762627997]
#
#def LagGauss(r,z,zR,lambda0,p):
#    return Gauss_w0(zR,lambda0)/Gauss_w(z,zR,lambda0)*np.exp(-r**2/Gauss_w(z,zR,lambda0)**2)*np.exp(-1j*k0*r**2/2/Gauss_R(z,zR))*scipy.special.eval_laguerre(p,2*r**2/Gauss_w(z,zR,lambda0)**2)*np.exp(1j*(2*p+1)*gouysPhase(z,zR))*np.exp(-1j*k0*z)
#
#def LGauss_E(r,z,zR,lambda0):
#    result=0
#    for i in range(len(lagcoeff)):
#        result+=lagcoeff[i]*LagGauss(r,z,zR,lambda0,i)
#    return result
#
#r = np.linspace(0,3e-6,1000)
#ax1 = plt.gca()
##Ed = lambda r: EFieldFiberMode(r,a,kT,gamma,s)
##ax1.plot(r,np.abs(Ed(r))**2/np.abs(Ed(0))**2)
#ax1.plot(r,np.abs(LGauss_E(r,0,zR_Fiber,lambda0))**2/np.abs(LGauss_E(0,0,zR_Fiber,lambda0))**2)
#
#z = 4e-3
#dx = np.tan(2*11.8/180*pi)*z
#
#fiberModeFarField = lambda r: LGauss_E(r,z,zR_Fiber,lambda0)/sqrt(2.0878564926789047e-11)
#
#plt.clf()
#ax1 = plt.gca()
#r = np.linspace(0,2e-3,1000)
#ax1.plot(r,np.abs(fiberModeFarField(r))**2/np.abs(fiberModeFarField(0))**2)
#
## Normalize
#a = scipy.integrate.quad(lambda rd: 2*pi*rd*fiberModeFarField(rd)*np.conjugate(fiberModeFarField(rd)), 0, 2e-3, epsabs=1e-14,epsrel=1e-14,limit=10000)[0]
#print(a)
#
#fiberModeFarField2D = lambda x, y: fiberModeFarField(sqrt(x**2+y**2))
#fiberModeFarField2DAbs = lambda x, y: fiberModeFarField2D(x,y)*np.conjugate(fiberModeFarField2D(x,y))
#
#fiberModeFarField2DOverlapRe = lambda x, y: np.real(fiberModeFarField2D(x+dx/2,y)*np.conjugate(fiberModeFarField2D(x-dx/2,y)))
#fiberModeFarField2DOverlapIm = lambda x, y: np.imag(fiberModeFarField2D(x+dx/2,y)*np.conjugate(fiberModeFarField2D(x-dx/2,y)))
#
#options={'limit':1000}
#overlapRe=scipy.integrate.nquad(fiberModeFarField2DOverlapRe,[[-3e-3,3e-3],[-2e-3,2e-3]],
#          args=(),opts=[options,options])
#overlapIm=scipy.integrate.nquad(fiberModeFarField2DOverlapIm,[[-3e-3,3e-3],[-2e-3,2e-3]],
#          args=(),opts=[options,options])
##b = scipy.integrate.dblquad(fiberModeFarField2D, -2e-3, 2e-3, lambda y: -2e-3, lambda y: 2e-3, epsabs=1e-10,epsrel=1e-10)
#
##fiberModeFarField2D = lambda x, y: LGauss_E(sqrt(x**2+y**2),z,zR_Fiber,lambda0)
#
##DiffIntReal = lambda rd,r: np.real(fiberModeFarField(r,rd,z,lambda0)*Ed(rd))
##DiffIntImag = lambda rd,r: np.imag(FresnelInt(r,rd,z,lambda0)*Ed(rd))
#
##a = scipy.integrate.quad(lambda rd: fiberModeFarField(rd)*np.conjugate(fiberModeFarField(rd)), 0, 2e-3, epsabs=1e-10,epsrel=1e-10,limit=10000)[0]
##b = scipy.integrate.quad(lambda rd: DiffIntImag(rd,ra), 0, Ed_lim, epsabs=1e-10,epsrel=1e-10,limit=10000)[0]
##    DiffE[i] = a+1j*b
