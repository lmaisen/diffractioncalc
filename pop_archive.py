# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 10:42:33 2020

@author: Lothar Maisenbacher/MPQ

Archive of various ZMEAX POP (physical optics propagation) results
"""

# B. Halle 3 lens collimator 2016
#filename = 'BHa_RefToPlane_486nm'
#filename = 'BHa_RefToPlane_410nm'
#filename = 'BHa_410nm_MFD3.6um'
#filename = 'BHa_410nm_MFD3um'
#filename = 'BHa_380nm_MFD2.5um'
#filename = 'BHa_486nm_MFD4.6um'
#filename = 'BHa_410nm_MFD3.6um_512auto'
#filename = 'BHa_410nm_MFD3.6um_1024auto'
#filename = 'BHa_410nm_MFD3.6um_2048auto'
#filename = 'BHa_410nm_MFD3.0um_2048auto'
#filename = 'BHa_486nm_MFD4.6um_2048auto'
#filename = 'BHa_380nm_MFD2.5um_2048auto'
# Vitaly
#filename = 'BH_410nm_FiberMode_512'
#filename = 'BH_410nm_Gauss1.8um_512'

# B. Halle 4 lens re-design 22.05.2017
#filename = 'BHaN1_410nm'
#filename = 'BHaN1_MC_WORST_410nm'
#filename = 'BHaN1_410nm_MFD3um'
#filename = 'BHa4L_410nm_MFD3.6um'
#filename = 'BHa4L_410nm_MFD3um'

# B. Halle 4 lens re-design V9 08.06.2017
#filename = 'BHa4LV9_410nm_MFD3.6um'
#filename = 'BHa4LV9_410nm_MFD3um'
#filename = 'BHa4LV9_380nm_MFD2.5um'
#filename = 'BHa4LV9_486nm_MFD4.6um'

# B. Halle 4 lens re-design V10 02.08.2017
#filename = 'BHaV10_410nm_MFD3.6um'
#filename = 'BHaV10_410nm_MFD3um'
#filename = 'BHaV10_486nm_MFD4.6um'
#filename = 'BHaV10_380nm_MFD2.5um'
#filename = 'BHaV10_410nm_MFD3.6um_2048auto'
#filename = 'BHaV10_410nm_MFD3.0um_2048auto'
#filename = 'BHaV10_380nm_MFD2.5um_2048auto'
#filename = 'BHaV10_486nm_MFD4.6um_2048auto'
# Toleranced version
#filename = 'BHaV10Tol1_410nm_MFD3.6um'
#filename = 'BHaV10Tol1_410nm_MFD3um'
#filename = 'BHaV10Tol1_380nm_MFD2.5um'
#filename = 'BHaV10Tol1_410nm_MFD3.6um_2048auto_x'
#filename = 'BHaV10Tol1_410nm_MFD3.6um_2048auto_y'
#filename = 'BHaV10Tol1_410nm_5000_MC_TOLRMC_WORST_16384auto'

# B. Halle 4 lens re-design 27.07.2017
# FS3
#filename = 'BHa4LFS3_410nm_MFD3.6um'
#filename = 'BHa4LFS3_410nm_MFD3um'
#filename = 'BHa4LFS3_380nm_MFD2.5um'
#filename = 'BHa4LFS3_380nm_MFD2.5um_2'
# FS5
#filename = 'BHa4LFS5_410nm_MFD3.6um'
#filename = 'BHa4LFS5_410nm_MFD3um'
#filename = 'BHa4LFS5_380nm_MFD2.5um'
#filename = 'BHa4LFS5_486nm_MFD4.6um'
# Hn2
#filename = 'BHa4LHn2_410nm_MFD3.6um'
#filename = 'BHa4LHn2_410nm_MFD3um'
#filename = 'BHa4LHn2_380nm_MFD2.5um'

# B. Halle 4 lens final design V11 14.09.2017
#filename = 'BHaV11_410nm_MFD3.6um_2048auto'
#filename = 'BHaV11_380nm_MFD2.5um_2048auto'
#filename = 'BHaV11_486nm_MFD4.6um_2048auto'
# Toleranced version 18.09.2017
#filename = 'BHaV11Tol1_410nm_MFD3.6um_2048auto_x'
#filename = 'BHaV11Tol1_410nm_MFD3.6um_2048auto_y'
# Misalignments
#filename = 'BHaV11_Mis_0' # 9.99922478e-01
#filename = 'BHaV11_Mis_L1_1deg' # Lens 1 tilted about y axis by 1 deg, backcoupling: 9.96327068e-01
#filename = 'BHaV11_Mis_L3_1deg' # Lens 3 tilted about y axis by 1 deg, backcoupling: 9.99885527e-01

# Two achromats (2S-4P collimator)
#filename = '2Ach_RefToPlane_486nm'
#filename = '2Ach_486nm_MFD4.6um'
#filename = '2Ach_410nm_MFD3.6um'
#filename = '2Ach_410nm_MFD3um'
#filename = '2Ach_380nm_MFD2.5um'

# Other collimators
#filename = 'AFL25-30_RefToPlane_410nm'
#filename = 'AL1225H_RefToPlane_410nm'

# Optimized three lens collimator
#filename = 'F30_3lenses_RefToPlane_410nm_MFD3um'
#filename = 'F30_3lenses_RefToPlane_410nm_MFD3.6um'

# Paraxial lens (as check)
#filename = 'Paraxial_RefToPlane_410nm'