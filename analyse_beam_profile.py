# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 17:24:46 2019

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec
from PIL import Image
import time

# pyhs
import pyhs.fit_2d_gaussian
import pyhs.fit

import pyha.plts as plts
plts.set_fontsize('FontsizeMed')

# Init fit class
myFit = pyhs.fit.Fit()

#%% Load beam profile measurement

input_folder = '..\\Beam profile measurements\\'
filename = '2019-08-09 15-04 410nm 0deg.tiff'
#filename = '2019-09-09 14-48 lens tissue in Faraday cage_.tiff'
# Pixel size in mm
pixel_size = 5.5e-3 # WinCamD-LCM
image_rot_90_deg_ccw = True # Rotating image by 90 deg CCW
imObj = Image.open(input_folder + filename)
im = np.array(imObj)
im = im.astype('uint64')
if image_rot_90_deg_ccw:
    im = np.swapaxes(im, 0, 1)
    im = np.flipud(im)
# Transpose such that x axis is axis 0, y axis is axis 1
im = im.transpose()
x_pixels = np.linspace(0, im.shape[0], im.shape[0])
y_pixels = np.linspace(0, im.shape[1], im.shape[1])
x = x_pixels * pixel_size
y = y_pixels * pixel_size
trim_ix = [0, im.shape[0]]
trim_iy = [0, im.shape[1]]
im_trim = im
x_pixels_trim = x_pixels
y_pixels_trim = y_pixels
x_trim = x
y_trim = y

#%% Fit 2D Gaussian

rot_fixed = True

myFit2DGaussian = pyhs.fit_2d_gaussian.fit2dgaussian()

start_time = time.time()
if rot_fixed:
    fit_result = myFit2DGaussian.fitgaussianRotFixed(im)
else:
    fit_result = myFit2DGaussian.fitgaussian(im)

# Convert fit results from pixels to SI units (m)
fit_result_si = fit_result.copy()
fit_result_si[1:5] = fit_result_si[1:5]*pixel_size

if rot_fixed:
    gauss_func_fit = myFit2DGaussian.gaussianRotFixed(*fit_result)
else:
    gauss_func_fit = myFit2DGaussian.gaussian(*fit_result)
im_fit = gauss_func_fit(*np.indices(im.shape))
im_fit_trim = im_fit
im_res = im_fit-im
im_res_trim = im_res

print('Computation time for 2D fit: {:.2f} s'.format(time.time()-start_time))

#%% Trim and center image to beam profile

#trim_lim = 5
trim_lim = 0.8
mask_x = (np.abs(x-fit_result_si[1]) < trim_lim)
trim_ix = [np.where(mask_x==True)[0][0], np.where(mask_x==True)[0][-1] + 1]
mask_y = (np.abs(y-fit_result_si[2]) < trim_lim)
trim_iy = [np.where(mask_y==True)[0][0], np.where(mask_y==True)[0][-1] + 1]
im_trim = im[trim_ix[0]:trim_ix[1], trim_iy[0]:trim_iy[1]]
im_fit_trim = im_fit[trim_ix[0]:trim_ix[1], trim_iy[0]:trim_iy[1]]
im_res_trim = im_res[trim_ix[0]:trim_ix[1], trim_iy[0]:trim_iy[1]]
x_pixels_trim = x_pixels[trim_ix[0]:trim_ix[1]]
y_pixels_trim = y_pixels[trim_iy[0]:trim_iy[1]]
x_trim = x[trim_ix[0]:trim_ix[1]]
y_trim = y[trim_iy[0]:trim_iy[1]]

#%% Plot 2D image and fit results

#plt.figure(figsize=(6, 12))
plt.clf()
gs = matplotlib.gridspec.GridSpec(3, 1,
    width_ratios=[1],
    height_ratios=[1, 1, 1]
    )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax3 = plt.subplot(gs[2])
axs = [ax1, ax2, ax3]

im_res_trim_norm = im_res_trim/im_fit_trim
dust_lim = [-0.8, 0.4]
#dust_lim = [-1, 0.1]
mask_dust = ((im_res_trim_norm < dust_lim[1])
    & (im_res_trim_norm > dust_lim[0]))
im_res_trim_norm[~mask_dust] = 0

for (ax, im_, label, cmap) in zip(
        axs,
        [im_trim, im_fit_trim, im_res_trim_norm],
        ['Intensity (arb. u.)', 'Intensity (arb. u.)', 'Norm. fit residuals'],
        ['gray', 'gray', 'coolwarm']
        ):

    h_im = ax.imshow(
        im_.transpose(),
        extent=[np.min(x_trim), np.max(x_trim), np.max(y_trim), np.min(y_trim)],
        cmap=cmap,
        )
    ax.set_ylabel('y (mm)')
    h_cbar = plt.colorbar(h_im, ax=ax)
    h_cbar.set_label(label)

axs[-1].set_xlabel('x (mm)')
[ax.set_xticklabels([]) for ax in axs[:-1]]

plt.suptitle(filename, fontsize=plts.params['FontsizeMed'])
plt.tight_layout()
plt.subplots_adjust(top=0.95)

#%% Show 1D cuts

#plt.figure(figsize=(6, 8))

# Prepare 1D fit
fit_func_id = 'GaussianBeam'
fit_func = myFit.fitFuncs[fit_func_id]

plt.clf()
gs = matplotlib.gridspec.GridSpec(2, 1,
    width_ratios=[1],
    height_ratios=[1,1]
    )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
axs = [ax1, ax2]

plot_pos_rel_to_peak = False
ix = int(fit_result[1])
iy = int(fit_result[2])
# Average over di pixels perpendicular to cut direction
di = 5

# Cut along x
cut_pixels_trim = x_pixels_trim
im_cut = np.mean(im[trim_ix[0]:trim_ix[1], iy-di:iy+di+1], axis=1)
im_fit_cut = np.mean(im_fit[trim_ix[0]:trim_ix[1], iy-di:iy+di+1], axis=1)
x0_2d = fit_result_si[1]
# Cut along y
#cut_pixels_trim = y_pixels_trim
#im_cut = np.mean(im[ix-di:ix+di+1, trim_iy[0]:trim_iy[1]], axis=0)
#im_fit_cut = np.mean(im_fit[ix-di:ix+di+1, trim_iy[0]:trim_iy[1]], axis=0)
#x0_2d = fit_result_si[2]
# Fit with 1D Gaussian
pstart = [fit_result[2], fit_result[4], np.max(im_cut), 0]
fit_gaussian, _ = myFit.doFit(
    fit_func, cut_pixels_trim, im_cut, np.ones(len(cut_pixels_trim)),
    warning_as_error=False,
    pstart=pstart,
    )
im_fit_1d_cut = fit_func['Func'](cut_pixels_trim, *fit_gaussian['Popt'])
sr_fit = myFit.fit_result_to_series(fit_gaussian, fit_func)
#x_cut = (x_cut_pixels-sr_fit['x0_Value'])*pixel_size
cut = (cut_pixels_trim)*pixel_size
if plot_pos_rel_to_peak:
    cut -= x0_2d
ax1.plot(
    cut,
    im_cut,
    label='Cut through data',
    )
h1_cut_fit = ax1.plot(
    cut,
    im_fit_cut,
    linewidth=2,
    label='Cut through 2D fit',
    )
h1_cut_fit_1d = ax1.plot(
    cut, im_fit_1d_cut,
    linestyle='-', linewidth=2,
    label='1D fit',
    )
#ax2.plot(
#    cut,
#    (im_cut - im_fit_cut)/im_fit_cut,
#    color=h1_cut_fit[0].get_color(),
#    )
ax2.plot(
    cut,
    (im_cut - im_fit_1d_cut)/im_fit_1d_cut,
    color=h1_cut_fit_1d[0].get_color(),
    )

ax1.legend(
    **plts.leg_params
    )
ax1.set_ylabel('Intensity (arb. u.)')
ax2.set_ylabel('Norm. fit residuals')

axs[-1].set_xlabel('x (mm)')
[ax.set_xticklabels([]) for ax in axs[:-1]]

plt.suptitle(filename, fontsize=plts.params['FontsizeMed'])
plt.tight_layout()
plt.subplots_adjust(top=0.95)
