# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 16:46:06 2019

@author: Lothar Maisenbacher/MPQ

Check influence of lens imperfections, modeled as phase mask at collimator
position, on field at interaction point.
"""

import numpy as np
import scipy.integrate
import scipy.special
import scipy.optimize
import scipy.interpolate
import matplotlib.pyplot as plt
import matplotlib.gridspec
import time
import LightPipes

# pyh
import pyh_helpers

# pyhs
import pyhs.fit

import fresnel_func

import pyha.plts as plts
plts.set_fontsize('FontsizeMed')

# Init fit class
myFit = pyhs.fit.Fit()

# Set display unit for length
unit = pyh_helpers.Units(1e-3, 'm')

#%% Global constants and settings

lambda0 = 410e-9

w0_Fiber = 3.6e-6/2 # Beam waist in fiber (MFD/2) at 410 nm

# Beam and collimator properties
f_Coll = 30e-3 # Focal length of collimator
zR_Fiber = fresnel_func.Gauss_zR(w0_Fiber, lambda0)
zR_Coll = fresnel_func.lens_zR(zR_Fiber, f_Coll)
w0_Coll = fresnel_func.Gauss_w0(zR_Coll, lambda0)

# Geometry
z_Mirror = 0.3 # Distance of HR mirror from collimator
dz_Interaction = 0.15 # Distance of interaction region from mirror, i.e. first interaction point at dz_Interaction after collimator, second interaction point at z_Mirror + dz_Interaction after collimator

#%% Define electric field at collimator

# Gaussian beam as input
#Ed_xy = lambda x, y: (
#    fresnel_func.Gauss_E(
#    np.sqrt(x**2+y**2), 0, zR_Coll, lambda0)
##    * np.exp(1j * 0.02 * np.cos(2*np.pi*x/0.35e-3))
#    )
delta = 0.185e-6 # Optimal position s = 0
Ed_xy = (
    lambda x, y:
        fresnel_func.Gauss_E(np.sqrt(x**2+y**2), -(f_Coll+delta), zR_Fiber, lambda0)
        * fresnel_func.lens_Phase(np.sqrt(x**2+y**2), -f_Coll, lambda0)
    * np.exp(1j * 0.02 * np.cos(2*np.pi*x/0.35e-3))
    )

#%% Calculate diffraction pattern

# Grid for numerical calculation
grid_size = 20e-3
grid_dimension = 1000
ix_center = int(grid_dimension/2)

# Set  LightPipes parameters
x = np.linspace(-grid_size/2, grid_size/2, grid_dimension)
r = x[ix_center:]
#lp_prop_func = LightPipes.Fresnel
lp_prop_func = LightPipes.Forvard

field_0 = LightPipes.Begin(grid_size, lambda0, grid_dimension)

# Initial electric field on grid
Y, X = np.meshgrid(x, x)
field_coll = LightPipes.Begin(grid_size, lambda0, grid_dimension)
field_coll.field = Ed_xy(X, Y)

# Diffraction calculations
start_time = time.time()
# Calculate diffracted field at mirror position
z = z_Mirror
field_3 = lp_prop_func(z, field_coll)
DiffE_3_lp = field_3.field[:, ix_center]
DiffEr_3_lp = DiffE_3_lp[ix_center:]

print('1 - overlap integral between forward (1) and backward (2) beams at mirror position: {:.5e}'
    .format(1-fresnel_func.calcOverlap(r, DiffEr_3_lp, np.conjugate(DiffEr_3_lp))))

# Calculate diffracted field at first interaction point (forward beam)
z = (z_Mirror - dz_Interaction)
field_1 = lp_prop_func(z, field_coll)

# Calculate diffracted field at second interaction point (backward beam)
z = (z_Mirror + dz_Interaction)
field_2 = lp_prop_func(z, field_coll)

print('Computation time for diffraction calculation: {:.2f} s'
    .format(time.time()-start_time))

#%% Plot and fit 1D cuts

# Prepare 1D fit
fit_func_id = 'GaussianBeam'
fit_func = myFit.fitFuncs[fit_func_id]

# Plot results
fig = plt.figure('Lens imperfections')
plt.clf()
gs = matplotlib.gridspec.GridSpec(
    3, 1,
    width_ratios=[1],
    height_ratios=[1, 1, 1]
    )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1], sharex=ax1)
ax3 = plt.subplot(gs[2], sharex=ax1)
axs = [ax1, ax2, ax3]

mask_x = np.ones(len(x), dtype=bool)
mask_x = np.abs(x) < 5e-3
for (field, label) in zip(
        [
         field_coll,
         field_1,
#         field_3,
         field_2,
#         (field_1+field_2*np.exp(1j*0.35*np.pi)),
#         (field_1+field_2*np.exp(1j*(0.35+1)*np.pi))
        ],
        [
         'Collimator',
         '1st interaction point',
#         'Mirror',
         '2nd interaction point',
#         'Difference at interaction point',
         'Difference at interaction point',
         ]
#        [field_3, field_1+field_2],
#        ['Mirror', 'Difference at interaction point'],
        ):
    inten = np.abs(field.field[:, ix_center])**2
    phase = np.unwrap(np.angle(field.field[:, ix_center]))
    inten_ = inten[mask_x]
    phase_ = phase[mask_x]
    # Subtract phase offset
    phase_ = phase_ - np.min(phase_)
    x_ = x[mask_x] * unit.scaleFactor
    # Fit with 1D Gaussian
    pstart = [0, w0_Coll*unit.scaleFactor, np.max(inten), 0]
    fit_gaussian, _ = myFit.doFit(
        fit_func, x_, inten_, np.ones(len(x_)),
        warning_as_error=False,
        pstart=pstart,
        )
    h_inten = ax1.plot(
        x_, inten_,
        label=label,
        )
    ax1.plot(
        x_,
        fit_func['Func'](x_, *fit_gaussian['Popt']),
        color=h_inten[0].get_color(),
        linestyle='--'
        )
    ax2.plot(
        x_,
        ((inten_-fit_func['Func'](x_, *fit_gaussian['Popt']))
        /fit_func['Func'](x_, *fit_gaussian['Popt'])),
        color=h_inten[0].get_color(),
        )
    ax3.plot(
        x_, phase_,
        color=h_inten[0].get_color(),
        )

ax1.legend(
    **plts.leg_params
    )

ax1.set_ylabel('Intensity (a.u.)')
ax2.set_ylabel('Norm. fit residuals')
ax3.set_ylabel('Phase (rad)')
axs[-1].set_xlabel('x ({})'.format(unit.unitName))

plt.tight_layout()

#%% Plot field in x-z plane, showing standing wave pattern

Ed_xz = (lambda z:
    (field_1.field[:, ix_center]+field_2.field[:, ix_center]*np.exp(1j*4*(z+0.35/4)*np.pi))
    )

z = np.linspace(0, 1, 500)
field_xz = np.array([Ed_xz(z_) for z_ in z])
im = np.abs(field_xz)**2

x_lim = 2e-3
mask_x = (np.abs(x) < x_lim)
ix_0 = np.where(mask_x==True)[0][0]
ix_1 = np.where(mask_x==True)[0][-1] + 1
im_trim = im[:, ix_0:ix_1]
x_trim = x[ix_0:ix_1]
#im_trim = im

fig = plt.figure('Standing wave pattern')
plt.clf()
ax1 = plt.gca()
h_im = ax1.imshow(
    im_trim/np.max(im_trim),
    extent=[np.min(x_trim), np.max(x_trim), np.max(z), np.min(z)],
    aspect='auto',
    )
ax1.set_xlabel('x (m)')
ax1.set_ylabel('z ($\lambda$)')
h_cbar = plt.colorbar(h_im, ax=ax1)
h_cbar.set_label('Norm. intensity')
plt.tight_layout()

#%% Plot 2D beam profile

fig = plt.figure('2D beam profile')
plt.clf()
gs = matplotlib.gridspec.GridSpec(
    2, 1,
    width_ratios=[1],
    height_ratios=[1, 1],
    figure=fig,
    )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
axs = [ax1, ax2]

x_lim = 5e-3
mask_x = (np.abs(x) < x_lim)
ix_0 = np.where(mask_x==True)[0][0]
ix_1 = np.where(mask_x==True)[0][-1] + 1

field_ = field_coll
title = 'Collimator'
#field_ = field_1
#title = '1st interaction point'
for (ax, im, label, cmap) in zip(
        axs,
        [np.abs(field_.field)**2, np.angle(field_.field)],
        ['Intensity (arb. u.)', 'Phase (rad)'],
        ['gray', 'viridis'],
        ):

    im_trim = im[ix_0:ix_1, ix_0:ix_1]
    x_trim = x[ix_0:ix_1] * unit.scaleFactor
    h_im = ax.imshow(
        im_trim.transpose(),
        extent=[np.min(x_trim), np.max(x_trim), np.max(x_trim), np.min(x_trim)],
        cmap=cmap,
        )
    ax.set_ylabel('y ({})'.format(unit.unitName))
    h_cbar = plt.colorbar(h_im, ax=ax)
    h_cbar.set_label(label)

axs[-1].set_xlabel('x ({})'.format(unit.unitName))
[ax.set_xticklabels([]) for ax in axs[:-1]]

plt.suptitle(title, fontsize=plts.params['FontsizeMed'])
plt.tight_layout()
plt.subplots_adjust(top=0.95)
