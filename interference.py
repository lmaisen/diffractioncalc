# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 19:02:28 2018

@author: Lothar
"""

import numpy as np
import scipy.constants

lambda0 = 410e-9
nu0 = scipy.constants.c/lambda0
w0 = 1e-6
k = 2*np.pi/lambda0
omega = 2*np.pi*nu0
#E = lambda x, y, t: np.real(np.exp(1j*(k*x-omega*t))) * np.exp(-y**2/w0**2)
#E = lambda x, y, t: np.exp(-y**2/w0**2) * (np.real(np.exp(1j*(k*x-omega*t)) + np.exp(1j*(-k*x-omega*t))))
alpha = 20/180*np.pi
alpha = np.arctan(1/2)
y0 = -1*w0
#E = lambda x, y, t: np.real(np.exp(-y**2/w0**2) * np.exp(1j*(k*x-omega*t)) + 1*np.exp(-((y*np.cos(alpha)-(x-x0)*np.tan(alpha))**2)/w0**2) * np.exp(1j*(k*((x-x0)*np.cos(alpha)+y*np.sin(alpha))-omega*t)))
E = lambda x, y, t: np.real(1*np.exp(-y**2/w0**2) * np.exp(1j*(k*x-omega*t)) + 1*np.exp(-y**2/w0**2) * np.exp(1j*(-k*x-omega*t)) + 1*np.exp(-((np.cos(alpha)*(y-y0)-np.sin(alpha)*x)**2)/w0**2) * np.exp(1j*(k*(x*np.cos(alpha)+(y-y0)*np.sin(alpha))-omega*t)))
#E = lambda x, y, t: np.exp(-((np.cos(alpha)*y-np.sin(alpha)*x)**2)/w0**2)
#E = lambda x, y, t: np.real(np.exp(-y**2/w0**2) * np.exp(1j*(k*x-omega*t)) + np.exp(-y**2/w0**2) * np.exp(1j*(-k*x-omega*t)))
#E = lambda x, y, t: np.real(np.exp(1j*(k*x-omega*t)) + np.exp(1j*(-k*x-omega*t)))
#E = lambda x, y, t: np.real(np.exp(1j*(k*x-omega*t)) + np.exp(1j*(-k*x-omega*t)) + 0*np.exp(1j*(k*x*np.cos(alpha)-omega*t)))
E2 = lambda x, y, t: np.abs(E(x,t))**2
I = lambda x, y: [scipy.integrate.quad(lambda t: np.abs(E(x0,y,t))**2, 0, 1/nu0)[0]*nu0 for x0 in x]
I_ = lambda x, y: np.array([ [scipy.integrate.quad(lambda t: np.abs(E(x0,y0,t))**2, 0, 1/nu0)[0]*nu0 for x0 in x] for y0 in y])

ax1 = plt.gca()
ax1.cla()
x = np.linspace(0, 4*w0,100)
y = np.linspace(-3*w0,3*w0,100)
t = np.linspace(0, 1/nu0,1000)
#ax1.plot(x,E(x,0))
#ax1.plot(x,E(x,1/nu0/4))
#ax1.plot(x,E(x,1/nu0/2))
#ax1.plot(x,E(x,0,0))

X,Y = np.meshgrid(x, y)
#Z = E(X, Y, 0)
Z = I_(x, y)
ax1.pcolormesh(X,Y,Z)

#ax1.plot(t,E2(0,t))
#print(scipy.integrate.quad(lambda s: E2(0,s), 0, 1/nu0)[0]*nu0)
#ax1.plot(x,I(x,0), linestyle='--')
#ax1.set_ylim([-0.1,4.1])