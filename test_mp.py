# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 14:14:23 2020

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import multiprocessing

class Test:

    detuning = np.nan
    test = np.nan

    def __init__(self):

        None

    def calc(self):

        out = self.test * self.detuning
        print(out)
        return out

for i in range(2):

    myTest = Test()
    myTest.test = i

    #class ParallelTest:
    #
    #    def __init__(self):
    #
    #        self.myTest = None
    #
    #    def solve(self, detuning):
    #
    #        print(detuning)
    #        self.myTest.detuning = detuning
    #        return myTest.calc()

    #myPTest = ParallelTest()
    #myPTest.myTest = myTest

    #myTest.detuning = 1
    #out = myTest.calc()
    #print(out)

    #myPTest.solve(2)

    if __name__ == "__main__":

        def psolve(detuning):

            print(detuning)
            myTest.detuning = detuning
            return myTest.calc()

        num_proc = 10
        detunings = np.arange(num_proc)
        with multiprocessing.Pool(processes=num_proc) as pool:

            out = pool.map(psolve, detunings)

        print(out)
