# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 14:59:42 2017

@author: Lothar Maisenbacher/MPQ

Evaluate overlap of reflections off of collimator with spectroscopy beam,
using results of Gaussian beam propagation of these reflections in Zemax as input.
For this, a new Zemax file is generated for each reflections
(can be done automatically within Zemax), then with a Zemax script each file
is opened and the Gaussian beam propagation parameters are found.
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import datetime

# pyhs
import pyhs.fit

import fresnel_func

# Init fit class
myFit = pyhs.fit.Fit()

#%% Analysis of ghost reflections from collimator

# Class that handles outputs
class printfc():

    output = []

    def add(self, addstr):

        print(addstr)
        self.output.append(addstr)

    def write(self, filepath):

        with open(filepath, 'w') as file_handler:
            for item in self.output:
                file_handler.write("{}\n".format(item))

output = printfc()

lambda0 = 410e-9
w0_Fiber = 3.6e-6/2 # Beam waist in fiber (MFD/2) at 410 nm
w0 = w0_Fiber
#w0 = 2e-3 # Beam waist of collimated beam at 410 nm
zR = fresnel_func.Gauss_zR(w0,lambda0)

folder = 'ZEMAXGBP'
model = '2Ach'
#model = 'V6'
#model = 'V9'
#model = 'FS3'
#model = 'FS5'
model = 'V11'
pos = 'AtFiber'
#pos = 'AtInter'
filename = model + '_' + pos + '.dat'
reflGauss = np.loadtxt(os.path.join(folder, filename), skiprows=1, delimiter=',')
output.add(filename)

# Beam and collimator properties
f_Coll = 30e-3 # Focal length of collimator
zR_Fiber = fresnel_func.Gauss_zR(w0_Fiber, lambda0)
zR_Coll = fresnel_func.lens_zR(zR_Fiber, f_Coll)
w0_Coll = fresnel_func.Gauss_w0(zR_Coll, lambda0)

Npoints = 100000 # Number of points to calculate
r = np.linspace(
    0, 50*w0_Coll, Npoints)
x = np.hstack((-np.flipud(r[1:]),r))

output.add('Overlap with beam with w_0 = {:.2e} mm\n'.format(w0*1e3))
fig_ga = plt.figure('Ghost analysis')
fig_ga.clf()
ax1 = fig_ga.gca()
overlaps = np.zeros(len(reflGauss))
for i, surf in enumerate(reflGauss):

    Ed = lambda rd: fresnel_func.Gauss_E(
        rd,
        surf[3]*1e-3,
        fresnel_func.Gauss_zR(surf[2]*1e-3, lambda0),
        lambda0)
    # Fit intensity with Gaussian
    fit_func_id = 'GaussianBeam'
    fit_func = myFit.fitFuncs[fit_func_id]
    pstart = [0, 1e-3, (np.abs(Ed(0))**2).max(), 0]
    ax1.plot(x, np.abs(Ed(x))**2)
    ax1.plot(x, fit_func['Func'](x, *pstart))

    fit_gaussian, _ = myFit.doFit(
        fit_func,
        x, np.abs(Ed(x))**2,
        pstart=pstart,
        warning_as_error=False
        )
    popt_gauss = fit_gaussian['Popt']

    output.add('Surface {:.0f}'.format(surf[0]))
#    output.add('Beam size at pos (ZEMAX): {:.2e} mm'.format(surf[1]))
    output.add(
        'Beam size at pos (calc): {:.2e} mm'.format(popt_gauss[1]*1e3))
    output.add('Rayleigh length: {:.2e} m'.format(
        fresnel_func.Gauss_zR(surf[2]*1e-3,lambda0)))
    overlaps[i] = fresnel_func.calcOverlap(
        r, Ed(r), fresnel_func.Gauss_E(r, 0, zR, lambda0))
    output.add('Overlap integral: {:.2e}'.format(overlaps[i]))

    ax1.plot(
        x,
        np.abs(Ed(x))**2/np.abs(Ed(0))**2,
        label=(
            r'$w_0$ = {:.2f} mm, $I_\mathrm{{max}}$ = {:.3e}'
            .format(popt_gauss[1]*1e3, popt_gauss[2])))

output.add('\nMin. overlap: {:.2e}'.format(np.min(overlaps)))
output.add('Mean overlap: {:.2e}'.format(np.mean(overlaps)))
output.add('Max. overlap: {:.2e}'.format(np.max(overlaps)))

# output.write(os.path.join(
#     folder,
#     datetime.datetime.utcnow().strftime('%Y-%m-%d') + '_results_' + filename))

#%% Print formatted overlaps

for overlap in np.flipud(np.sort(overlaps)):
    print('{:.1e}'.format(overlap))

#%% Double reflections

#Npoints = 10000 # Number of points to calculate
#r = np.linspace(0,3*w0_Coll,Npoints)
#x = np.hstack((-np.flipud(r[1:]),r))
#
## V9 5 then 3
#Ed1 = lambda rd: Gauss_E(rd,2.79e-2,Gauss_zR(1.43e-6, lambda0),lambda0) # 3
##Ed2 = lambda rd: Gauss_E(rd,2.76e-2,Gauss_zR(1.22e-6, lambda0),lambda0) # 9
#Ed2 = lambda rd: Gauss_E(rd,1.07e-1,Gauss_zR(4.60e-6, lambda0),lambda0) # 7
## V9 6 then 4
##Ed1 = lambda rd: Gauss_E(rd,2.34e-2,Gauss_zR(9.71e-7, lambda0),lambda0) # 6
##Ed2 = lambda rd: Gauss_E(rd,7.08e-2,Gauss_zR(4.62e-6, lambda0),lambda0) # 11
##Ed1 = lambda rd: Gauss_E(rd,5.42e-2,Gauss_zR(3.69e-6, lambda0),lambda0) # 4
##Ed2 = lambda rd: Gauss_E(rd,1.19e-2,Gauss_zR(9.76e-7, lambda0),lambda0) # 8
#print('Overlap integral: {:.2e}'.format(calcOverlap(r,Ed1(r),Ed2(r))))
#ax1 = plt.gca()
#ax1.cla()
#ax1.plot(x,np.abs(Ed1(x))**2)
#ax1.plot(x,np.abs(Ed2(x))**2)
