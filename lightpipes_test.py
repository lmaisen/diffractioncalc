# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 10:38:12 2019

@author: Lothar Maisenbacher/MPQ

Test LightPipes module.
"""

import LightPipes
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

import fresnel_func

# Beam and collimator properties
lambda0 = 410e-9
w0_Fiber = 3.6e-6/2 # Beam waist in fiber (MFD/2) at 410 nm
f_Coll = 30e-3 # Focal length of collimator
zR_Fiber = fresnel_func.Gauss_zR(w0_Fiber, lambda0)
zR_Coll = fresnel_func.lens_zR(zR_Fiber, f_Coll)
w0_Coll = fresnel_func.Gauss_w0(zR_Coll, lambda0)

# Grid for numerical calculation
grid_size = 20e-3
grid_dimension = 1000
ix_center = int(grid_dimension/2)

# Set  LightPipes parameters
#x = np.linspace(-grid_size/2, grid_size/2, grid_dimension)
x_ = np.arange(-grid_size/2, grid_size/2, grid_size/grid_dimension)
#x = x - np.mean(x)

x=[]
for i in range(grid_dimension):
    x.append((-grid_size/2+i*grid_size/grid_dimension))
x = np.array(x)

##%%

field_0 = LightPipes.Begin(grid_size, lambda0, grid_dimension)
#field_0 = np.ones(
#    (grid_dimension, grid_dimension), dtype=np.complex)

#Field=Interpol(GridSize,NewGridDimension,0,0,0,1,Field)
#I = np.array(Intensity(0,Field))

# Gaussian beam as input
Ed_xy = lambda x, y: fresnel_func.Gauss_E(
    np.sqrt(x**2+y**2), 0, zR_Coll, lambda0)
X, Y = np.meshgrid(x, x)
field_0.field = Ed_xy(X, Y)
field = field_0

# Gaussian aperture as input
R = w0_Coll/np.sqrt(2)
T = 1
field_ = LightPipes.GaussAperture(R, 0, 0, T, field_0)

##%%

#GridDimension = 2048
#GridSize = 10*mm
#Field = Begin(GridSize, lambda0, GridDimension)
#Field = CircAperture(2*mm, 0, 0, Field)
##Field_forvard = Forvard(0.4*m, Field)
field_fresnel = LightPipes.Forvard(zR_Coll, field)

ax1 = plt.gca()
ax1.cla()

#I = np.array(Intensity(0, Field_forvard))
#ax1.plot(x, I[int(GridDimension/2)])
I = np.array(LightPipes.Intensity(0, field))
ax1.plot(x, I[ix_center])

I = np.array(LightPipes.Intensity(0, field_fresnel))
ax1.plot(x, I[ix_center])

#ax1.set_xlim([-5, 5])
ax1.set_xlabel('x (m)')
ax1.set_ylabel('Intensity (a.u.)')
#X, Y = np.meshgrid(range(grid_dimension), range(grid_dimension))
#fig=plt.figure(figsize=(10,6))
#ax = fig.gca(projection='3d')
#ax.plot_surface(X, Y,I,
#                rstride=1,
#                cstride=1,
#                cmap='rainbow',
#                linewidth=0.0,
#                )
#ax.set_zlabel('Intensity [a.u.]')
