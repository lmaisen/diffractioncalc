# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 00:09:17 2016

@author: Lothar Maisenbacher/MPQ
"""

import time
import scipy.integrate
import scipy.optimize
import scipy.interpolate
import numpy as np
import pandas as pd
import scipy.constants
import itertools
import matplotlib.pyplot as plt
import os

# pyhs
import pyhs.fitfunc
import pyhs.fit

import cOBE_2SnP as OBE_2SnP

import pyha.plts as plts
plts.set_params()

myFit = pyhs.fit.Fit()

#%% Open figure

fig_results_obe = plt.figure(
    'OBE results', figsize=(plts.pagewidth, 4), constrained_layout=True)
fig_results_obe.clf()

#%% Load OBE results

input_dir = 'obe_results'
filename_prefixes = [
    '2020-08-24 16-53-02 Gaussian_s=0',
#    '2020-08-26 18-12-55 Gaussian_s=-0.3',
    ]
resultsOBE = pd.DataFrame()
for i, filename_prefix in enumerate(filename_prefixes):
    resultsOBE_ = pd.read_csv(
        os.path.join(input_dir, filename_prefix + ' resultsOBE.dat'),
        index_col=0)
    resultsOBE = resultsOBE.append(resultsOBE_, ignore_index=True)
    if i==0:
        scan_detunings = np.load(
            os.path.join(input_dir, filename_prefix + ' detunings.npy'))
        scan_results = np.load(
            os.path.join(input_dir, filename_prefix + ' scanresults.npy'))
    else:
        scan_detunings_ = np.load(
            os.path.join(input_dir, filename_prefix + ' detunings.npy'))
        scan_detunings = np.vstack((scan_detunings, scan_detunings_))
        scan_results_ = np.load(
            os.path.join(input_dir, filename_prefix + ' scanresults.npy'))
        scan_results = np.vstack((scan_results, scan_results_))

#%% Sort results

#column = 'TransverseVelocity'
#sort_ = np.argsort(resultsOBE[column])
#resultsOBE = resultsOBE.iloc[sort_]
#scan_detunings = scan_detunings[sort_]
#scan_results = scan_results[sort_]

#%% Plot result for multiple resonances

fig_results_obe.clf()
ax1 = fig_results_obe.gca()

#active_fit_func_id = 'Lorentzian'
#active_fit_func_id = 'VoigtDoublet'
active_fit_func_id = 'Voigt'

mask = pd.Series(True, index=resultsOBE.index)
#mask = (
#    (np.round(resultsOBE['AlphaOffset'], 3) == 4.825)
#    )
print('Found {:d} fit result(s)'.format(np.sum(mask)))

for i, (_, result) in enumerate(resultsOBE[mask].iterrows()):
    scan_iid = result.name
    alpha_offset = result['AlphaOffset']
    oderesults = scan_results[scan_iid]
    signal = oderesults[:, -1, int(result['SignalColumnLyman'])]
    detuning = scan_detunings[scan_iid]
    mask = np.ones(len(detuning), dtype=bool)
#    mask[0] = False
    x = detuning[mask]
    y = signal[mask]
    ysigma = np.ones(len(y))

    plot_style = {
        **plts.point_params,
        'linestyle': '--', 'linewidth': 0.5,
        }

    # Plot result
    h1_data = ax1.plot(
        x, y,
        **plot_style,
        label='$P$ = {:.1f} µW, $v_z$ = {:.1f} m/s, $v_x$ = {:.2f} m/s'.format(
            result['LineSimPower'],
            result['Vz'],
            result['TransverseVelocity']
            )
        )

    # Show fit to line
    fit_func_id_add = 'VoigtDoublet'
    fit_func_id_add_2 = 'VoigtDoubletEqualAmps'
    delta_nu = 19e6
    if alpha_offset > 5:
        pstarts_ = {
            'CFR1': -delta_nu/2,
            'CFR2': delta_nu/2,
            'GammaL': 5e6,
            'GammaG': 3e6,
            'Amp1': np.max(y),
            'Amp2': np.max(y),
            'Offset': 0.,
            }
    else:
        pstarts_ = {
            'DeltaNu': 1e6,
            }
    line_fits = myFit.fitLine(
        x, y, np.ones(len(y)),
        out_func=print,
        fit_func_ids=['Lorentzian', 'Voigt', fit_func_id_add, fit_func_id_add_2],
        pstarts={
            'VoigtDoublet': pstarts_,
            'VoigtDoubletEqualAmps': pstarts_,
        },
#        debug=True,
        )
    fit_func = myFit.fit_funcs[active_fit_func_id]
#    fit_result = myFit.sr_to_fit_result(result, fit_func)
    fit_result = line_fits[active_fit_func_id]
    fit_result_dict = myFit.fit_result_to_dict(fit_result, fit_func)
    x_fit = np.linspace(np.min(x), np.max(x), 1000)
    h1_fit = ax1.plot(
        x_fit,
        fit_func['Func'](x_fit, *fit_result['Popt']),
        **plts.line_params,
        color=h1_data[0].get_color(),
        label='{}: {:.3e} Hz'.format(
            active_fit_func_id, fit_result_dict['CFR_Value'])
        )

#    print(-result['TransverseVelocity']/myOBE.lambda_laser)
    print('{:.4f} Hz'.format(fit_result_dict['CFR_Value']))
#    print(fit_result_dict['AmpRatio_Value'])
#    print(fit_result_dict)

ax1.set_xlabel('Laser detuning (Hz)')
ax1.set_ylabel('Signal (photons/atom)')
ax1.legend(**plts.get_leg_params())

#%%

fig_results_obe.clf()
ax1 = fig_results_obe.gca()

mask = (
    pd.Series(True, index=resultsOBE.index)
    & (resultsOBE['Delta'] == resultsOBE['Delta0'])
    )

ax1.plot(
    resultsOBE[mask]['AlphaOffset'],
    resultsOBE[mask]['CFR_Value'],
    **plts.get_point_params(**{
        'linestyle': '-',
        }),
    )

#%% Plot fit results as function of parameter

fig_results_obe.clf()
ax1 = fig_results_obe.gca()

mask = (
    pd.Series(True, index=resultsOBE.index)
#    & (resultsOBE['LineSimPower'] == 30)
    )

x_param = 'Delta'
y_param = 'CFR_Value'

columns = [
    'LineSimPower',
    'AlphaOffset'
    ]
unique_params = resultsOBE[mask].drop_duplicates(subset=columns)
for _, unique_param in unique_params.iterrows():
    mask_unique = mask.copy()
    for column in columns:
        mask_unique &= (resultsOBE[column]==unique_param[column])

    # Plot result
    x = resultsOBE[mask_unique][x_param]
    #y = resultsOBE[y_param + '_Value']
    y = resultsOBE[mask_unique][y_param]
    h1_data = ax1.plot(
        x,
        y,
        **plts.get_point_params(**{
            'linestyle': '-',
            }),
        )

#ax1.legend()
