# -*- coding: utf-8 -*-
"""
Created on Wed Aug  2 15:46:32 2017

@author: Vitaly Wirthl/MPQ

Generates a .zbf file containing an electric field.
This file can then be imported into the phyical optics propagation (POP) of Zemax.
"""

import os
import numpy as np
import scipy.special as special

cwd = os.getcwd()

outputFolder = cwd+'\\ZEMAXZBF\\' #Windows

if not os.path.exists(outputFolder):
    os.makedirs(outputFolder)
#%%
fileName = 'testPython'

NSamples=128 # Number of Samples
spacing=1.49e-2 #spacing between points in mm

f = open(outputFolder +  fileName +'.zbf', 'w')
f.write('A\n')  # A: indicates a text file
f.write('1\n')  # version: The format version number, currently 1.
f.write('%i\n'%NSamples)  # nx: The number of x samples.
f.write('%i\n'%NSamples)  # ny: The number of y samples.
f.write('0\n')  # ispol: The "is polarized" flag; 0 for unpolarized, 1 for polarized.
f.write('0\n') # units: 0 for mm, 1 for cm, 2 for in, 3 for meters.
for i in range(4):
    f.write('0\n') #unused
f.write('%.5E\n'%spacing) # dx: The x direction spacing between points.
f.write('%.5E\n'%spacing) #dy: The y direction spacing between points.
for i in range(18):
    f.write('0\n') #not important, pilot beam will be calculated anyways
## Now follows the definition of Ex/Ey real and imag. values, the imag. values are set to 0
waist=0.1
xvals=[]
yvals=[]
for i in range(NSamples):
    for j in range(NSamples):
        x=-(NSamples-1)/2*spacing+i*spacing
        xvals.append(x)
        y=-(NSamples-1)/2*spacing+j*spacing
        yvals.append(y)
        val=np.exp(-(x**2+y**2)/waist**2)
        f.write('%.5E\n'%val)
        f.write('0\n')

f.close()

#print(NSamples/2*spacing)
#print(xvals[0])

#%%
fileName = 'Gaussian 1.8um'

NSamples=512 # Number of Samples
spacing=0.072190894/512 #spacing between points in mm

f = open(outputFolder +  fileName +'.zbf', 'w')
f.write('A\n')  # A: indicates a text file
f.write('1\n')  # version: The format version number, currently 1.
f.write('%i\n'%NSamples)  # nx: The number of x samples.
f.write('%i\n'%NSamples)  # ny: The number of y samples.
f.write('0\n')  # ispol: The "is polarized" flag; 0 for unpolarized, 1 for polarized.
f.write('0\n') # units: 0 for mm, 1 for cm, 2 for in, 3 for meters.
for i in range(4):
    f.write('0\n') #unused
f.write('%.5E\n'%spacing) # dx: The x direction spacing between points.
f.write('%.5E\n'%spacing) #dy: The y direction spacing between points.
for i in range(18):
    f.write('0\n') #not important, pilot beam will be calculated anyways
## Now follows the definition of Ex/Ey real and imag. values, the imag. values are set to 0
waist=1.8e-3
xvals=[]
yvals=[]
for i in range(NSamples):
    for j in range(NSamples):
        x=-NSamples/2*spacing+i*spacing
        xvals.append(x)
        y=-NSamples/2*spacing+j*spacing
        yvals.append(y)
        val=np.exp(-(x**2+y**2)/waist**2)
        f.write('%.5E\n'%val)
        f.write('0\n')

f.close()


#%%

def Gauss_zR(w0,lambda0):
    return np.pi*w0**2/lambda0

def Gauss_w0(zR,lambda0):
    return np.sqrt(zR*lambda0/np.pi)

def Gauss_w(z,zR,lambda0):
    return Gauss_w0(zR,lambda0)*np.sqrt(1+(z/zR)**2)

def Gauss_R(z,zR):
    if z==0:
        return np.inf
    else:
        return z*(1+(zR/z)**2)

def GouyPhase(z,zR):
    return np.arctan(z/zR)

def Gauss_E(r,z,zR,lambda0):
    k0 = 2*np.pi/lambda0
    return Gauss_w0(zR,lambda0)/Gauss_w(z,zR,lambda0)*np.exp(-r**2/Gauss_w(z,zR,lambda0)**2)*np.exp(-1j*k0*r**2/2/Gauss_R(z,zR))*np.exp(1j*GouyPhase(z,zR))*np.exp(-1j*k0*z)

lambda0 = 410e-9
k0 = 2*np.pi/lambda0

#410nm: use w0f=1.703e-6
w0_Fiber=1.703e-6
lagcoeff=[2.1362844610291947,0.017182575299230802,0.10749844665878426,-0.07069515744311301,-0.019833354214996164,-0.039726501848900926,-0.012171424556575591,-0.011242833018581993,0.0015588379818807356,0.002713149312489355,0.007211929945945706,0.006446388334504639,0.006933819288008384,0.005167670043219531,0.00417056688775183,0.002359510288427045,0.0011794426439560239,-0.0001673536882193498,-0.0009888505227512228,-0.0017332598150315473,-0.0020768853762627997]

def LagGauss(r,z,zR,lambda0,p):
    return Gauss_w0(zR,lambda0)/Gauss_w(z,zR,lambda0)*np.exp(-r**2/Gauss_w(z,zR,lambda0)**2)*np.exp(-1j*k0*r**2/2/Gauss_R(z,zR))*special.eval_laguerre(p,2*r**2/Gauss_w(z,zR,lambda0)**2)*np.exp(1j*(2*p+1)*GouyPhase(z,zR))*np.exp(-1j*k0*z)

def LGauss_E(r,z,zR,lambda0):
    result=0
    for i in range(len(lagcoeff)):
        result+=lagcoeff[i]*LagGauss(r,z,zR,lambda0,i)
    return result

zR_Fiber = Gauss_zR(w0_Fiber,lambda0)

#fileName = 'Fiber mode S405-XP 410nm, N=512, W=0.072'
#NSamples=512 # Number of Samples
#spacing=0.072190894/NSamples #spacing between points in mm

#fileName = 'Fiber mode S405-XP 410nm, N=512, W=0.3'
#NSamples=512 # Number of Samples
#spacing=0.3/NSamples #spacing between points in mm

#fileName = 'Fiber mode S405-XP 410nm, N=512, W=0.01'
#NSamples=512 # Number of Samples
#spacing=0.01/NSamples #spacing between points in mm

#fileName = 'Fiber mode S405-XP 410nm, N=512, W=0.5'
#NSamples=512 # Number of Samples
#spacing=0.5/NSamples #spacing between points in mm

#fileName = 'Fiber mode S405-XP 410nm, N=512, W=0.9'
#NSamples=512 # Number of Samples
#spacing=0.9/NSamples #spacing between points in mm

#fileName = 'Fiber mode S405-XP 410nm, N=1024, W=0.102'
#NSamples=1024 # Number of Samples
#spacing=0.10209334/NSamples #spacing between points in mm

fileName = 'Fiber mode S405-XP 410nm, N=2048, W=0.144'
NSamples=2048 # Number of Samples
spacing=0.14438179/NSamples #spacing between points in mm

f = open(outputFolder +  fileName +'.zbf', 'w')
f.write('A\n')  # A: indicates a text file
f.write('1\n')  # version: The format version number, currently 1.
f.write('%i\n'%NSamples)  # nx: The number of x samples.
f.write('%i\n'%NSamples)  # ny: The number of y samples.
f.write('0\n')  # ispol: The "is polarized" flag; 0 for unpolarized, 1 for polarized.
f.write('0\n') # units: 0 for mm, 1 for cm, 2 for in, 3 for meters.
for i in range(4):
    f.write('0\n') #unused
f.write('%.5E\n'%spacing) # dx: The x direction spacing between points.
f.write('%.5E\n'%spacing) #dy: The y direction spacing between points.
for i in range(18):
    f.write('0\n') #not important, pilot beam will be calculated anyways
## Now follows the definition of Ex/Ey real and imag. values, the imag. values are set to 0
xvals=[]
yvals=[]
for i in range(NSamples):
    for j in range(NSamples):
        x=-NSamples/2*spacing+i*spacing
        xvals.append(x)
        y=-NSamples/2*spacing+j*spacing
        yvals.append(y)
        val=np.real(LGauss_E(np.sqrt((x*1e-3)**2+(y*1e-3)**2),0,zR_Fiber,lambda0))
        f.write('%.5E\n'%val)
        f.write('0\n')

f.close()

#%% Take fiber mode directly

def EFieldFiberMode(r,a,kT,gamma,s):
    result=0.0
    if r<=a:
        result=special.j0(kT*r)
    else:
        result=s*special.k0(gamma*r)
    return result


#410nm:
a=1.5e-6
kT=1.07920201798e6
gamma=1.08803993162e6
s=2.46475741617

#fileName = 'Fiber mode bessel S405-XP 410nm, N=512, W=0.072'
#NSamples=512 # Number of Samples
#spacing=0.072190894/NSamples #spacing between points in mm

#fileName = 'Fiber mode bessel S405-XP 410nm, N=1024, W=0.102'
#NSamples=1024 # Number of Samples
#spacing=0.10209334/NSamples #spacing between points in mm

fileName = 'Fiber mode bessel S405-XP 410nm, N=2048, W=0.144'
NSamples=2048 # Number of Samples
spacing=0.14438179/NSamples #spacing between points in mm

f = open(outputFolder +  fileName +'.zbf', 'w')
f.write('A\n')  # A: indicates a text file
f.write('1\n')  # version: The format version number, currently 1.
f.write('%i\n'%NSamples)  # nx: The number of x samples.
f.write('%i\n'%NSamples)  # ny: The number of y samples.
f.write('0\n')  # ispol: The "is polarized" flag; 0 for unpolarized, 1 for polarized.
f.write('0\n') # units: 0 for mm, 1 for cm, 2 for in, 3 for meters.
for i in range(4):
    f.write('0\n') #unused
f.write('%.5E\n'%spacing) # dx: The x direction spacing between points.
f.write('%.5E\n'%spacing) #dy: The y direction spacing between points.
for i in range(18):
    f.write('0\n') #not important, pilot beam will be calculated anyways
## Now follows the definition of Ex/Ey real and imag. values, the imag. values are set to 0
xvals=[]
yvals=[]
for i in range(NSamples):
    for j in range(NSamples):
        x=-NSamples/2*spacing+i*spacing
        xvals.append(x)
        y=-NSamples/2*spacing+j*spacing
        yvals.append(y)
        val=np.real(1e6*EFieldFiberMode(np.sqrt((x/1e3)**2+(y/1e3)**2),a,kT,gamma,s))
        f.write('%.5E\n'%val)
        f.write('0\n')

f.close()

#%%
